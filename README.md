# LSTetris Servidor

Descripció del projecte

## Getting Started

Aquesta guia t'ajudarà a descarregar el projecte per programar des de la teva màquina.

### Prerequisits

Abans de començar, es necessita:

1. [Git](https://git-scm.com/downloads)
2. [IntelliJ](https://www.jetbrains.com/idea/download) (o algún altre IDE que es prefereixi)

### Clonar

Es procedeix a clonar el projecte. Per tal de fer-ho ens movem al directori on hi vulguem la nostra carpeta amb el projecte a dins. 
Després amb la consola de comandes escrivim la seguent línia.

```
git clone https://gitlab.com/LSTetris/LSTetrisServidor.git
```

### config.json

El fitxer anomenat *config.json* ha d'anar a l'arrel d'el projecte des d'on es carregarà informació necessària per l'execució.

```
{
    "db_port" : 3306,
    "db_ip" : "localhost",
    "db_name" : "Tetris",
    "db_user" : "root",
    "db_password" : "2324",
    "client_port" : 12345
}
```
Cada desenvolupador ha de canviar els camps que cregui convenients per a que s'adaptin a la seva configuració.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

