DROP DATABASE IF EXISTS Tetris;
CREATE DATABASE Tetris;

USE Tetris;
CREATE TABLE Jugador (
    nickname VARCHAR(255),
    correu VARCHAR(255),
    contrasenya VARCHAR(255),
    estat_connectat BOOL,
    data_registre DATE,
    ultim_login TIMESTAMP,
    num_partides INT,
    punts INT,
    ascii_move_left VARCHAR(255),
    ascii_move_right VARCHAR(255),
    ascii_move_down VARCHAR(255),
    ascii_turn_left VARCHAR(255),
    ascii_turn_right VARCHAR(255),
    ascii_pause VARCHAR(255),
    PRIMARY KEY (nickname)
);

CREATE TABLE Partida(
	id_partida INT NOT NULL auto_increment,
    nickname_jugador VARCHAR(255),
    punts INT,
    data_inici DATE,
    pic_maxim_espectadors INT,
    Foreign Key (nickname_jugador) references Jugador (nickname),
    PRIMARY KEY (id_partida)
);
