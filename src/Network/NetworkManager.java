package Network;

import Controller.WindowController;
import Database.DatabaseManager;
import Model.Match;
import Model.User;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * La classe NetworkManager implementa el gestor de xarxa del servidor del LSTetris
 */
public class NetworkManager extends Thread{

    //El camp IP no s'utilitza mai
    //private final String IP = "127.0.0.1";
    private int PORT;
    private ServerSocket serverSocket;
    private final ArrayList<DedicatedServer> dedicatedServers;
    private WindowController controller;
    private boolean running;
    private DatabaseManager databaseManager;

    /**
    * Crea un nou NetworkManager
     * @param PORT es un enter que conte el port de connexio
     * @param databaseManager es el gestor de base de dades
     */
    public NetworkManager(int PORT, DatabaseManager databaseManager){
        this.dedicatedServers = new ArrayList<>();
        this.running = false;
        serverSocket = null;
        this.PORT = PORT;
        this.databaseManager = databaseManager;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(PORT);
            running = true;

            controller.updateRegister("Acceptant clients");

            while (running) {
                System.out.println("Waiting for a client...");
                Socket socket = serverSocket.accept();
                controller.updateRegister("Client connectat");
                DedicatedServer dServer = new DedicatedServer(socket, controller, this, databaseManager);
                dedicatedServers.add(dServer);
                dServer.start();

            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Acaba la conexio
     */
    public void stopServerConnection () {
        broadcastAll("Closing server");
        running = false;
        interrupt();
    }

    /**
     * Envia un missatge a tots els servidos dedicats
     * @param msg es el string que conte el missatge que es vol enviar
     */
    public void broadcastAll(String msg){
        for (DedicatedServer ds : dedicatedServers) {
            ds.sendMessage(msg);
        }
    }

    /**
     * Registra el controlador principal al NetworkManager
     * @param controller es el controlador del NetworkManager
     */
    public void registerController(WindowController controller) {
        this.controller = controller;
    }

    /**
     * Elimina una connexio amb un client de la llista de connexions
     * @param dedicatedServer es el servidor dedicat que ha de deixar de proporcionar servei
     */
    public void removeClient(DedicatedServer dedicatedServer){
        controller.updateRegister("Client has disconnected");
        dedicatedServer.stopClientConnection();
        dedicatedServers.remove(dedicatedServer);
    }

    /**
     * Gestiona l'informacio rebuda en un missatge per un servidor dedicat i executa les comandes adients.
     * @param information es el missatge que s'ha rebut en el servidor dedicat
     * @param dedicatedServer es el servidor dedicat
     */
    public void handleMessage(StatusMessage information, DedicatedServer dedicatedServer) {
        switch(information.getArguments()[0]){
            case "Information":
                switch(information.getArguments()[1]){
                    case "Screen":
                        System.out.println((String)information.getInformation());
                        break;
                }
                break;
            case "Game":
                switch(information.getArguments()[1]){
                    case "Start":
                        dedicatedServer.startMatch();
                        for (DedicatedServer ds: dedicatedServers) {
                            sendPossibleSpectateGames(ds);
                        }
                        break;
                    case "Key":
                        dedicatedServer.keyPressed((String) information.getInformation());
                        break;
                    case "EndNoSave":
                        dedicatedServer.endMatch(false);
                        for (DedicatedServer ds: dedicatedServers) {
                            sendPossibleSpectateGames(ds);
                        }
                        break;
                    case "EndSave":
                        dedicatedServer.endMatch(true);
                        for (DedicatedServer ds: dedicatedServers) {
                            sendPossibleSpectateGames(ds);
                        }
                        break;

                }
                break;

            //Missatges de gestió d'usuaris
            case "User":
                switch (information.getArguments()[1]){
                    case "Register":
                        //Rebem un usuari del client
                        User auxUser = (User) information.getInformation();
                        //Cridem el mètode del controlador que l'afegirà
                        controller.registerNewUser(auxUser, dedicatedServer);
                        dedicatedServer.setUser(auxUser.getNickname());
                        dedicatedServer.registerNickname(auxUser.getNickname());
                        break;

                    case "Login":
                        String [] infoUser = (String[]) information.getInformation();
                        //Rebem una petició de login i busquem si és correcte
                        if(infoUser[0].contains("@")){
                            infoUser[0] = databaseManager.getUsernameByEmail(infoUser[0]);
                        }
                        controller.loginUser(infoUser, dedicatedServer);
                        dedicatedServer.setUser(infoUser[0]);
                        controller.loginUser((String[]) information.getInformation(), dedicatedServer);
                        dedicatedServer.registerNickname(((String[]) information.getInformation())[0]);
                        break;
                }
                break;

            //Noves tecles per jugar
            case "KeyBinding":

                ArrayList<String> newKeys = (ArrayList<String>) information.getInformation();
                databaseManager.changeAssignedKeys(newKeys, dedicatedServer.getUser());

                break;

            //Desconnectar client
            case "Disconnect":
                dedicatedServer.stopClientConnection();
                dedicatedServers.remove(dedicatedServer);
                controller.updateRegister("Client has disconnected");
                //Actualitzem la base de dades i la taula de gestió
                controller.disconnectUser((String) information.getInformation());
                break;

            //Espectejar un altre jugador
            case "Spectator":
                switch (information.getArguments()[1]) {
                    //Començar a espectejar
                    case "Spectate":
                        //System.out.println(dedicatedServer.getUser() + " vol espectejar a " + (String)information.getInformation());
                        getDedicatedServerByUsername((String)information.getInformation()).addSpectator(dedicatedServer);

                        break;
                    //Demanar informació de les partides que hi ha en progrés
                    case "Players":
                        sendPossibleSpectateGames(dedicatedServer);
                        break;

                    //Parar espectejar una partida
                    case "StopSpectate":
                        getDedicatedServerByUsername((String)information.getInformation()).removeSpectator(dedicatedServer);
                        break;
                }
                break;

            //Mirar una partida antiga guardada
            case "Review":
                switch (information.getArguments()[1]){
                    //Parar de mirar partides guardades
                    case "StopReview":
                        dedicatedServer.stopReview();
                        break;

                    //Començar a mirar una partida guardada
                    case "Start":
                        Gson g = new Gson();

                        try {
                            JsonReader reader = new JsonReader(new FileReader("matches" + File.separator + dedicatedServer.getUser() + File.separator + information.getInformation()));
                            Match[] data = g.fromJson(reader, Match[].class);
                            dedicatedServer.startReviewing(data);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        break;

                    //Demanar informació de les partides disponibles per mirar
                    case "Info":
                        ArrayList<String> names = controller.getFileNames(information.getArguments()[2]);
                        dedicatedServer.sendObject(new String[]{"Review", "Info"}, names);
                        break;
                }
        }
    }

    /**
     * Mètode que envia a tots els clients que aquest està jugant, o no, i és elegible per espectejar, o no.
     * @param dedicatedServer Aquest dedicated server que està començant o parant de jugar
     */
    private void sendPossibleSpectateGames(DedicatedServer dedicatedServer){
        ArrayList<ArrayList<String>> infoUsers = new ArrayList<>();
        for (DedicatedServer ds: dedicatedServers) {
            if (ds.getGameThread() != null && ds.isPlaying()){
                ArrayList<String> tmp = new ArrayList<String>();
                tmp.add(ds.getUser());
                tmp.add(ds.getViewers());
                tmp.add(ds.getTimePlayed());
                infoUsers.add(tmp);
            }
        }
        dedicatedServer.sendObject(new String[]{"Spectator", "Players"}, infoUsers);
    }

    /**
     * Mètode que a partir d'un nom d'usuari retorna el DedicatedServer
     * @param username String amb el nom del DedicatedServer a buscar
     * @return DedicatedServer
     */
    private DedicatedServer getDedicatedServerByUsername(String username){
        for (DedicatedServer ds: dedicatedServers) {
            if(ds.getUser().equals(username)){
                return ds;
            }
        }

        return null;
    }
}
