package Network;

/**
 * La classe StatusMessage permet l'estructuracio dels missatges de comunicacio entre clients i servidors dedicats
 */
public class StatusMessage implements java.io.Serializable {
    private String[] arguments;
    private Object information;

    /**
     * Crea un nou StatusMessage
     * @param arguments son els arguments del missatge que seran interpretats per entendre que es el que s'esta rebent
     * @param information es l'objecte que s'esta enviant
     */
    public StatusMessage(String[] arguments, Object information) {
        this.arguments = arguments;
        this.information = information;
    }

    /**
     * Retorna tots els arguments del StatusMessage
     * @return els arguments
     */
    public String[] getArguments() {
        return arguments;
    }

    /**
     * Retorna l'objecte enviat
     * @return l'objecte enviat
     */
    public Object getInformation() {
        return information;
    }
}
