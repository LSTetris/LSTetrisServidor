package Network;

import Controller.WindowController;
import Database.DatabaseManager;
import Model.*;
import com.google.gson.Gson;

import java.awt.event.KeyEvent;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * La classe dedicated server proporciona connectivitat a un client de forma concurrent a altres connexions que hi hagi
 */
public class DedicatedServer extends Thread {
    private final ArrayList<DedicatedServer> spectators;
    private final Socket socket;
    private String user;
    //private final MessageManager model;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private WindowController controller;
    private String userName;
    private boolean running;
    private NetworkManager networkManager;
    private Match match;
    private GameThread gameThread;
    private DatabaseManager databaseManager;
    private ReviewThread reviewThread;

    /**
     * Crea un nou DedicatedServer
     * @param socket es el socket que cal utilitzar per la connexio
     * @param controller es el controlador principal del servidor
     * @param networkManager es el gestor de xarxa del servidor
     * @param databaseManager es el gestor de bases de dades del servidor
     */
    public DedicatedServer(Socket socket,
                           WindowController controller,
                           NetworkManager networkManager,
                           DatabaseManager databaseManager) {
        this.controller = controller;
        this.socket = socket;
        this.spectators = new ArrayList<>();
        this.networkManager = networkManager;
        this.databaseManager = databaseManager;
    }

    @Override
    public void run() {
        try {
            ois = new ObjectInputStream(socket.getInputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());

            running = true;

            while (running) {
                StatusMessage information = (StatusMessage)ois.readObject();
                networkManager.handleMessage(information, this);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            //En cas d'error desconnectar
            this.stopClientConnection();
        }
    }

    /**
     * Atura la connexio amb el client
     */
    public void stopClientConnection(){
        if(this.gameThread != null) {
            this.gameThread.stopGameThread();
        }
        running = false;
        interrupt();
    }

    /**
     * Envia un missatge al client
     * @param msg es el missatge que s'enviara
     */
    public void sendMessage(String msg){
        try {
            oos.writeObject(new StatusMessage(new String[]{"Information", "Screen"}, msg));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia un objecte al client
     * @param arguments son els arguments que donen informació al client sobre que rebra
     * @param o es l'objecte que es vol enviar
     */
    public void sendObject(String[] arguments, Object o){
        try {
            oos.writeObject(new StatusMessage(arguments, o));
            oos.reset();
        } catch (IOException e) {
            e.printStackTrace();
            this.stopClientConnection();
        }
    }

    /**
     * Comença una nova partida.
     */
    public void startMatch() {
        this.match = new Match();
        this.gameThread = new GameThread(this, match, databaseManager, spectators);
        this.gameThread.registerUser(userName);
        this.gameThread.restartMatch();
        this.gameThread.start();
    }

    public void startReviewing(Match[] partida){
        this.reviewThread = new ReviewThread(partida, this);
        this.reviewThread.start();
    }

    /**
     * Gestiona l'input de teclat del client
     * @param key la tecla premuda pel client
     */
    public void keyPressed(String key) {
        this.gameThread.keyPressed(key);
    }

    /**
     * Estableix l'usuari que empra el servidor
     * @param user es l'usuari que es vol connectar
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Retorna l'usuari que empra el servidor
     * @return l'usuari que está connectat
     */
    public String getUser() {
        return user;
    }

    /**
     * Retorna el fil d'execucio del joc
     * @return el fil d'execucio del joc
     */
    public GameThread getGameThread() {
        return gameThread;
    }

    /**
     * Estableix el nickname de l'usuari
     * @param s es el nickname de l'usuari
     */
    public void registerNickname(String s) {
        this.userName = s;
    }

    public void endMatch(boolean saveMatch) {
        if(saveMatch){
            System.out.println("Guardant partida");

            int numFitxer = databaseManager.saveGame(getUser(), this.match.getScore(), this.gameThread.getMaxSpectators());
            String json = new Gson().toJson(this.gameThread.getPartidaGuardada());

            try (PrintWriter out = new PrintWriter("matches" + File.separator + getUser() + File.separator + numFitxer + ".json")) {
                out.println(json);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            this.gameThread.endMatch();
            this.gameThread.stopGameThread();
        }else{
            this.gameThread.endMatch();
            this.gameThread.stopGameThread();
        }

    }

    public boolean isPlaying(){
        return this.gameThread.isRunning();
    }

    public String getViewers() {
        return String.valueOf(this.gameThread.getSpectators());
    }

    public String getTimePlayed() {
        return String.valueOf(this.gameThread.calculateTime());
    }

    public void addSpectator(DedicatedServer spectator) {
        gameThread.addSpectators();
        spectators.add(spectator);
    }


    public void removeSpectator(DedicatedServer spectator) {
        spectators.remove(spectator);
    }

    public void stopReview() {
        this.reviewThread.stopReview();
    }
}