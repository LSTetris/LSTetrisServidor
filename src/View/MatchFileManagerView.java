package View;

import Controller.WindowController;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.Vector;

/**
 * Classe de la vista encarregada d'ensenyar els fitxers que hi ha per cada usuari i gestionar-los.
 */
public class MatchFileManagerView extends JFrame{

    public final static String ACTCMD_BTN_DEL = "BTN_ADM_DEL";
    public final static String ACTCMD_BTN_REF = "BTN_ADM_RED";
    public final static String ACTCMD_BTN_BCK = "BTN_ADM_BCK";
    public final static String ACTCMD_BTN_LD = "BTN_ADM_LD";

    private JButton jbDelete;
    private JButton jbNavigate;
    private JButton jbRefresh;

    private JList jListFiles;

    private JLabel jlTitle;
    private TitledBorder title;

    /**
     * Mètode constructor de la classe. Rep un vector de String amb els noms dels usuaris que tenen carpetes.
     * @param names Vector de String que conté els noms dels usuaris per poder accedir a les seves carpetes.
     */
    public MatchFileManagerView(Vector<String> names){

        this.setLayout(new BorderLayout());

        //Carreguem el JList
        jListFiles = new JList(names);


        jlTitle = new JLabel("Selecciona l'usuari");
        jlTitle.setFont(jlTitle.getFont().deriveFont(15.0f));
        this.add(jlTitle, BorderLayout.NORTH);

        jbRefresh = new JButton("Refrescar");
        jbDelete = new JButton("Eliminar");
        jbNavigate = new JButton("Entrar");
        jbDelete.setEnabled(false);


        JPanel jpNav = new JPanel(new GridLayout(1, 2));
        jpNav.add(jbNavigate);
        jpNav.add(jbDelete);

        JPanel jpBottomAux = new JPanel(new GridLayout(2, 1));

        jpBottomAux.add(jpNav);
        jpBottomAux.add(jbRefresh);


        title = BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.black), "Usuaris");
        jListFiles.setBorder(title);


        this.add(jpBottomAux, BorderLayout.SOUTH);
        this.add(jListFiles, BorderLayout.CENTER);


        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setSize(300, 400);
        this.setTitle("Partides Guardades");

    }

    public String getSelectedElement(){
        return (String) jListFiles.getSelectedValue();
    }


    /**
     * Carrega les partides d'un usuari en concret
     * @param filenames Vector amb la informació a carregar pel JList
     * @param userName Nom d'usuari amb el que es canviarà el títol del border
     */
    public void loadUserMatches(Vector<String> filenames, String userName){

        if (userName != null) {
            jListFiles.setListData(filenames);
            title.setTitle("Partides de " + userName);
            jlTitle.setText("Partides");

            //Canviem la funció del botó de carregar un usuari a la de anar enrere
            jbNavigate.setLabel("Tornar");
            jbNavigate.setActionCommand(ACTCMD_BTN_BCK);
            //Activem el botó d'eliminar
            jbDelete.setEnabled(true);
        }

    }

    /**
     * Carrega les carpetes de cada usuari
     * @param filenames Vector amb la informació a carregar pel JList
     */
    public void loadUsers(Vector<String> filenames){

        jListFiles.setListData(filenames);
        title.setTitle("Usuaris");
        jlTitle.setText("Selecciona l'usuari");

        //Canviem la funció del botó de navegació per la de carregar usuari
        jbNavigate.setLabel("Entrar");
        jbNavigate.setActionCommand(ACTCMD_BTN_LD);
        jbDelete.setEnabled(false);
    }


    public void registerController(WindowController controller){

        jbNavigate.addActionListener(controller);
        jbDelete.addActionListener(controller);
        jbRefresh.addActionListener(controller);

        jbDelete.setActionCommand(ACTCMD_BTN_DEL);
        jbRefresh.setActionCommand(ACTCMD_BTN_REF);
        jbNavigate.setActionCommand(ACTCMD_BTN_LD);
    }

}
