package View;

import Model.StatisticSet;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * JPanel que pinta un gràfic de barres.
 * Donat un ArrayList de double que representa els valors de cada barra i un ArrayList de String que representa el nom dels jugadors,
 * dibuixa un gràfic de barres escalat en que els noms dels jugadors apareixen sota cada barra i la seva puntuació dins la barra.
 *
 * @author Grup C1
 */
public class BarPanel extends JPanel {
    //Espai entre el gràfic i els bordes
    private static final int PADDING = 25;
    //Espai per les etiquetes
    private static final int LABEL_PADDING = 25;
    //Color de les barres
    private static final Color BAR_FILL_COLOR = Color.green;
    //Color del contorn de les barres
    private static final Color BAR_DRAW_COLOR = Color.black;
    //Color de les lletres
    private static final Color FONT_COLOR = Color.black;
    //Color del fons del grafic
    private static final Color BACKGROUND_COLOR = Color.white;
    //Pinzell utilitzat
    private static final Stroke GRAPH_STROKE = new BasicStroke(2f);
    //Tamany de la font
    private static final double FONT_SCALE = 0.20;

    //Valors de les barres
    private ArrayList<Double> values;
    //Noms dels jugadors
    private ArrayList<String> names;

    /**
     * Crea un nou BarPanel a partir d'un ArrayList de valors i un ArrayList de noms
     * @param values son els valors a representar
     * @param names son els noms dels jugadors
     */
    public BarPanel(ArrayList<Double> values, ArrayList<String> names) {
        this.values = values;
        this.names = names;
    }

    /**
     * Crea un nou BarPanel a partir de un StatisticSet que son les dades a representar.
     * @param set son les dades a representar
     */
    public BarPanel (StatisticSet set) {
        this.values = set.getNumbers();
        this.names = set.getNames();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        double xScale = ((double) getWidth() - 2 * PADDING - LABEL_PADDING) / (values.size());
        double yScale = ((double) getHeight() - 2 * PADDING - LABEL_PADDING) / (getMaxScore() - getMinScore());
        int ground = getHeight() - PADDING - LABEL_PADDING;

        double fontSize = xScale * FONT_SCALE;
        Font defaultFont = new Font("Arial", Font.PLAIN, (int)Math.round(fontSize));

        ArrayList<Double> bars = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            double y1 = (getMaxScore() - Math.round(values.get(i))) * yScale + PADDING;
            bars.add(y1);
        }

        // Draw white background
        g2.setColor(BACKGROUND_COLOR);
        g2.fillRect(PADDING + LABEL_PADDING, PADDING, getWidth() - (2 * PADDING) - LABEL_PADDING, getHeight() - 2 * PADDING - LABEL_PADDING);

        // Draw X-axis names
        double x0 = PADDING + LABEL_PADDING;
        double y0 = ground + 3;
        for (int i = 0; i < names.size(); i++) {
            g2.setColor(FONT_COLOR);
            g2.setFont(defaultFont);
            String xLabel = names.get(i);
            FontMetrics metrics = g2.getFontMetrics();
            int labelWidth = metrics.stringWidth(xLabel);
            g2.drawString(xLabel, (int)Math.round(x0 + xScale/2 - labelWidth/2), (int)Math.round(y0+ metrics.getHeight()));
            x0+=xScale;
        }

        // Create x and y axes
        g2.drawLine(PADDING + LABEL_PADDING, getHeight() - PADDING - LABEL_PADDING, PADDING + LABEL_PADDING, PADDING);
        g2.drawLine(PADDING + LABEL_PADDING, getHeight() - PADDING - LABEL_PADDING, getWidth() - PADDING, getHeight() - PADDING - LABEL_PADDING);

        g2.setColor(BAR_FILL_COLOR);
        g2.setStroke(GRAPH_STROKE);

        //Draw the filled bard and the numbers
        for (int i = 0; i < bars.size(); i++) {
            x0 = i * xScale + PADDING + LABEL_PADDING;
            y0 = bars.get(i);
            double height = ground - y0;

            g2.setColor(BAR_DRAW_COLOR);
            g2.drawRect((int)Math.round(x0), (int)Math.round(y0), (int)Math.round(xScale), (int)Math.round(height ));

            g2.setColor(BAR_FILL_COLOR);
            g2.fillRect((int)Math.round(x0), (int)Math.round(y0), (int)Math.round(xScale), (int)Math.round(height) );

            g2.setColor(FONT_COLOR);
            g2.setFont(defaultFont);
            String numeral =Integer.toString((int)Math.round(values.get(i)));
            FontMetrics metrics = g2.getFontMetrics();
            int labelWidth = metrics.stringWidth(numeral);
            g2.drawString(numeral, (int)Math.round(x0 + xScale/2 - labelWidth/2), (int)Math.round(y0 + PADDING));
        }

    }

    /**
     * Retorna el minim valor dels que s'utilitzen a les barres.
     * @return es el minim valor de les barres.
     */
    private double getMinScore() {
        double minScore = 0;
        for (double score : values) {
            minScore = Math.min(minScore, score);
        }
        return minScore;
    }

    /**
     * Retorna el maxim valor dels que s'utilitzen en les barres.
     * @return es el maxim valor de les barres.
     */
    private double getMaxScore() {
        double maxScore = 0;
        for (double score : values) {
            maxScore = Math.max(maxScore, score);
        }
        return maxScore;
    }

    /**
     * Acctualitza les dades que s'utilitzen per dibuixar el gràfic.
     * @param set son les dades que s'utilitzen per dibuixar el gràfic.
     */
    public void updateGraph(StatisticSet set) {
        this.values = set.getNumbers();
        this.names = set.getNames();
    }
}