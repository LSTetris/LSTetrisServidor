package View;

import Model.StatisticSet;

import javax.swing.*;
import java.awt.*;

/**
 * JFrame que mostra les estadistiques de les partides.
 * Aixo inclou les partides més visualitzades de la ultima setmana i les partides amb mes punts de la ultima setmana.
 *
 * @author: Grup C1
 */
public class GraphView extends JFrame {
    //El gràfic de barres de les partides amb més punts
    private BarPanel bpPoints;
    //El gràfic de barres de les partides amb més espectadors
    private BarPanel bpViewers;

    /**
     * Crea un nou JFrame amb dos Tabs on a cada tab hi ha un gràfic de barres.
     * @param points son les dades pel gràfic de barres de les partides amb més punts de la última setmana.
     * @param viewers son les dades pel gràfic de barres de les partides amb més espectadors de la última setmana.
     */
    public GraphView(StatisticSet points, StatisticSet viewers){
        JTabbedPane jtpStatistics = new JTabbedPane();
        JScrollPane jspPoints = new JScrollPane();
        JScrollPane jspViewers = new JScrollPane();

        bpPoints = new BarPanel( points);
        bpViewers = new BarPanel( viewers);

        jtpStatistics.addTab("Top punts", bpPoints);
        jtpStatistics.addTab("Top viewers", bpViewers);

        this.add(jtpStatistics);

        //Propietats que calen posar a la finestra
        this.setSize(500, 400);
        this.setMinimumSize(new Dimension(600, 300));
        this.setTitle("Grafics Usuari");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    /**
     * Actualitza les dades dels gràfics.
     * @param points son les dades pel gràfic de barres de les partides amb més punts de la última setmana.
     * @param viewers son les dades pel gràfic de barres de les partides amb més espectadors de la última setmana.
     */
    public void update (StatisticSet points, StatisticSet viewers) {
        bpPoints.updateGraph(points);
        bpViewers.updateGraph(viewers);
    }

}
