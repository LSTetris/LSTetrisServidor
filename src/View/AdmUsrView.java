package View;

import Controller.ViewWindowController;
import Controller.WindowController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;

/**
 * La classe AdmUsrView implementa una interficie grafica per a poder gestionar els usuaris de forma més usable.
 * Aquesta vista conte una taula on cada fila es un usuari i cada columna es una dada diferent de l'usuari.
 */
public class AdmUsrView extends JFrame {

    //Action commands accessibles des de qualsevol classe
    public final static String ACTCMD_BTN_GAMES = "ACTCMD_BTN_GAMES";
    public final static String ACTCMD_DEL_USER = "ACTCMD_DEL_USER";

    private DefaultTableModel tableModel;
    private JTable jtbRegisteredUsers;
    private JButton jbDelete;

    /**
     * Crea una nova AdmUsrView.
     */
    public AdmUsrView(){

        //JTable amb tots amb els usuaris registrats
        tableModel = new DefaultTableModel(0, 6);
        tableModel.setColumnIdentifiers(new String[]{"Connectat", "Nom", "Data de registre", "Ultim inici de sessió", "Partides jugades", "Punts totals"});
        jtbRegisteredUsers = new JTable(tableModel);

        JScrollPane jScrollPaneUsers = new JScrollPane(jtbRegisteredUsers);
        jScrollPaneUsers.setBorder(BorderFactory.createTitledBorder("Gestió d'usuaris registrats"));

        this.setLayout(new BorderLayout());
        this.add(jScrollPaneUsers, BorderLayout.CENTER);

        jbDelete = new JButton("Esborrar usuari");
        this.add(jbDelete, BorderLayout.SOUTH);


        //Propietats que calen posar a la finstra
        this.setSize(500, 400);
        this.setMinimumSize(new Dimension(600, 300));
        this.setTitle("Administar Jugadors");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    /**
     * Registra un controlador a la vista.
     * @param controller es el controlador de la vista
     */
    public void registerController(WindowController controller){
        jbDelete.setActionCommand(ACTCMD_DEL_USER);
        jbDelete.addActionListener(controller);
    }

    /**
     * Carrega les dades dels usuaris a la vista.
     * @param users son les dades dels usuaris
     */
    public void loadUserData(String[][] users){
        if (users.length > 0) {
            for (String[] str_array : users) {
                tableModel.addRow(new Object[]{str_array[0], str_array[1], str_array[2], str_array[3], str_array[4], str_array[5]});
            }
        }
    }

    /**
     * Retorna la fila seleccionada a la vista
     * @return la fila seleccionada
     */
    public int getSelectedRow(){
        return jtbRegisteredUsers.getSelectedRow();
    }

    /**
     * Retorna el nom d'una fila concreta
     * @param id es l'identificacio de la fila
     * @return el nom de la fila
     */
    public String getRowName(int id){
        return (String) tableModel.getValueAt(id, 1);
    }

    /**
     * Esborra una fila concreta de la vista
     * @param id es l'identificador de la fila a esborrar
     */
    public void deleteRowById(int id){
        tableModel.removeRow(id);
    }

    /**
     * Esborra totes les files de la taula de la vista
     */
    public void clearTable(){
        while(tableModel.getRowCount() > 0){
            tableModel.removeRow(0);
        }
    }

}
