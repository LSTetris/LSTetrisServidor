/*
  MainWindow
  Tetrix
  Servidor
  05/04/2018
 */

package View;

import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Classe Vista del MVC que fa herència del JFrame per usar les característiques de Swing. Es fa servir
 * un JSplitPane per separar la vista en dues columnes i distribuir millor la informació. Els botons obren unes
 * finestres independents a aquesta.
 */
public class MainView extends JFrame {

    //Action commands accessibles des de qualsevol classe
    public final static String ACTCMD_BTN_ADM = "BTN_ADM_USR";
    public final static String ACTCMD_BTN_STAT = "BTN_ADM_STAT";
    public final static String ACTCMD_BTN_FLS = "BTN_ADM_FLS";

    //Components de Swing necessaris per implementar les funcions del servidor
    private JButton jButtonStatistics;
    private JButton jButtonAdmUsers;
    private JButton jButtonFiles;
    private JTextArea jTextAreaInfo;

    /**
     * Mètode constructor de la vista. Primer s'instancien els panells i després els components. Un cop creats
     * es modifiquen les propietats i es van col·locant.
     */
    public MainView() {

        /* Panells */

        //Panell auxiliar de la dreta amb BoxLayout
        JPanel jPanelRightAux = new JPanel();
        jPanelRightAux.setLayout(new BoxLayout(jPanelRightAux, BoxLayout.PAGE_AXIS));


        //Panell auxiliar per als botons amb un FlowLayout per posar-los de costat
        JPanel jPanelButtonsAux = new JPanel(new FlowLayout());
        jPanelButtonsAux.setMaximumSize(new Dimension(500, 500));

        /* Altres components */

        //Botons
        jButtonStatistics = new JButton("Estadístiques");
        jButtonAdmUsers = new JButton("Administrar Usuaris");
        jButtonFiles = new JButton("Gestió de fitxers");

        //Afegim els botons al panell
        jPanelButtonsAux.add(jButtonAdmUsers);
        jPanelButtonsAux.add(jButtonStatistics);
        jPanelButtonsAux.add(jButtonFiles);

        //El JTextArea amb la informació del que està passant al servidor
        jTextAreaInfo = new JTextArea("Inicialitzant servidor");
        jTextAreaInfo.setEditable(false);


        //El JScrollPane del JTextArea per si es tornés massa gran
        JScrollPane jScrollPaneInfoAux = new JScrollPane(jTextAreaInfo);
        jScrollPaneInfoAux.setBorder(BorderFactory.createTitledBorder("Registre Servidor"));


        //Afegim els components al JPanel de la dreta
        jPanelRightAux.add(jPanelButtonsAux);
        jPanelRightAux.add(jScrollPaneInfoAux);

        //Li posem un BorderLayout a aquesta finestra
        this.setLayout(new BorderLayout());

        //Afegim el JSplitPane amb tots els components a aquesta vista
        this.add(jPanelRightAux, BorderLayout.CENTER);

        //Propietats que calen posar a la finstra
        this.setSize(600, 400);
        this.setTitle("LSTetris NetworkManager");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    /**
     * Mètode que enllaça els botons amb el controlador de manera que aquest sàpiga què ha de gestionar.
     * @param controller ActionListener que ha de gestionar els esdeveniments.
     * @param viewController ViewWindowController que ha de gestionar els esdeveniments.
     */
    public void registerController(ActionListener controller, ViewWindowController viewController){
        //Registrem els ActionCommands als botons
        jButtonAdmUsers.setActionCommand(ACTCMD_BTN_ADM);
        jButtonStatistics.setActionCommand(ACTCMD_BTN_STAT);
        jButtonFiles.setActionCommand(ACTCMD_BTN_FLS);

        //Fem que l'ActionListener respongui als esdeveniments dels botons
        jButtonAdmUsers.addActionListener(controller);
        jButtonStatistics.addActionListener(controller);
        jButtonFiles.addActionListener(controller);

        this.addWindowListener(viewController);
    }

    /**
     * Mètode que actualitza la informació del que està passant al servidor. Simplement s'actualitza
     * el text del JTextArea concatenant el missatge que li arriba.
     * @param order String amb la informació.
     */
    public void updateRegister(String order){
        this.jTextAreaInfo.setText(jTextAreaInfo.getText() + "\n" + order);
    }


}

