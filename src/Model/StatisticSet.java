package Model;

import java.util.ArrayList;

/**
 * La classe StatisticSet s'encarrega d'agrupar les dades que cal mostrar per coneixer les estadistiques de les partides des del servidor.
 */
public class StatisticSet {
    private ArrayList<Double> numbers;
    private ArrayList<String> names;

    /**
     * Crea un nou StatisticSet sense dades
     */
    public StatisticSet () {
        numbers = new ArrayList<Double>();
        names = new ArrayList<String>();
    }

    /**
     * Retorna els valors de cada columna estadistica
     * @return els valors estadistics
     */
    public ArrayList<Double> getNumbers() {
        return numbers;
    }

    /**
     * Retorna els noms de cada columna estadistica
     * @return els noms de les columnes
     */
    public ArrayList<String> getNames() {
        return names;
    }

    /**
     * Estableix els valors del StatisticSet
     * @param names son els noms de les columnes
     * @param numbers son els valors de les columnes
     */
    public void setValues(ArrayList<String> names, ArrayList<Double> numbers) {
        this.numbers = numbers;
        this.names = names;
    }


    @Override
    public String toString () {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numbers.size(); i++) {
            sb.append(numbers.get(i) + " ,");
        }
        sb.append(System.lineSeparator());
        for (int i = 0; i < names.size(); i++) {
            sb.append(names.get(i) + " ,");
        }

        return sb.toString();
    }
}
