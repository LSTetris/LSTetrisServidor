package Model;

import Network.DedicatedServer;

/**
 * Classe que envia cada 60ms l'estat de la partida que està reproduint un client.
 */
public class ReviewThread extends Thread {

    private DedicatedServer dedicatedServer;
    private Match[] partida;
    private boolean stop;

    /**
     * Constructor que rep la partida i el client que la vol veure.
     * @param partida Match[] amb les dades de la partida
     * @param dedicatedServer DedicatedServer a on enviar l'estat
     */
    public ReviewThread(Match[] partida, DedicatedServer dedicatedServer){
        this.partida = partida;
        this.dedicatedServer = dedicatedServer;
    }

    /**
     * Mètode que es crida quan es fa el start(), mentres no s'estigui al final de la partida anar enviant el següent
     * estat al client. Si s'activa stop vol dir que el client ha sortit de la finestra de reproducció i que no
     * cal que s'envii més.
     */
    @Override
    public void run(){

        stop = false;

        for(int i = 0; i < partida.length && !stop; i++){
            try {
                Thread.sleep(60);
                dedicatedServer.sendObject(new String[]{"Review", "Status"}, partida[i]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void stopReview() {
        stop = true;
    }
}
