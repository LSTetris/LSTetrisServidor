package Model;

/**
 * La classe figura implementa les funcions comuns a totes les peces del LSTetris.
 */
public class Figure {
    //Les posicions de les peces que la composen
    private char[][] pieces;

    private int numFigure;
    private int numPosition;
    private int boardPosX;
    private int boardPosY;

    //Enter amb la posició de l'array on hi ha el primer bloc per l'esquerra de la peça per facilitar el càlcul de col·lisions
    private int colliderLeftCol;

    //Enter amb la posició de l'array on hi ha el darrer bloc per la dreta de la peça per facilitar el càlcul de col·lisions
    private int colliderRightCol;

    //Enter amb la posició de l'array on hi ha el darrer bloc per sota la peça per facilitar el càlcul de col·lisions
    private int colliderBottomRow;

    //Enter amb la posició de l'array on hi ha el primer bloc per dalt de la peça per facilitar el càlcul de col·lisions
    private int colliderTopRow;

    /**
     * Crea una nova figura.
     * @param pieces es la matriu amb les posicions de les peces.
     * @param numFigure es el numero de la figura.
     * @param position es la posicio de la figura.
     * @param colliderBottomRow es el limit inferior de la peça
     * @param colliderTopRow es el limit superior de la peça
     * @param colliderLeftCol es el limit esquerra de la peça
     * @param colliderRightCol es el limit dreta de la peça
     * @param boardPosX es la posicio horizontal de la peça
     * @param boardPosY es la posicio vertical de la peça
     */
    public Figure(char[][] pieces,
                  int numFigure,
                  int position,
                  int colliderBottomRow,
                  int colliderTopRow,
                  int colliderLeftCol,
                  int colliderRightCol,
                  int boardPosX,
                  int boardPosY){
        this.pieces = pieces;
        this.numFigure = numFigure;
        this.numPosition = position;
        this.colliderBottomRow = colliderBottomRow;
        this.colliderTopRow = colliderTopRow;
        this.colliderLeftCol = colliderLeftCol;
        this.colliderRightCol = colliderRightCol;
        this.boardPosX = boardPosX;
        this.boardPosY = boardPosY;
    }

    /**
     * Retorna les peces que formen la figura.
     * @return matriu de oeces que formen la figura
     */
    public char[][] getPieces() {
        return pieces;
    }

    /**
     * Estableix les peces que formen la figura.
     * @param pieces son les peces que formen la figura.
     */
    public void setPieces(char[][] pieces) {
        this.pieces = pieces;
    }

    /**
     * Retorna el numero de la figura
     * @return el numero de la figura
     */
    public int getNumFigure() {
        return numFigure;
    }

    /**
     * Estableix el numero de la figura
     * @param numFigure es el numero de la figura
     */
    public void setNumFigure(int numFigure) {
        this.numFigure = numFigure;
    }

    /**
     * Retorna el numero de posicio de la figura.
     */
    public int getNumPosition() {
        return numPosition;
    }

    /**
     * Estableix la posicio de la figura
     * @param numPosition es la posicio de la figura
     */
    public void setNumPosition(int numPosition) {
        this.numPosition = numPosition;
    }

    /**
     * Retorna la posicio horizontal de la figura
     * @return la posicio horizontal de la figura
     */
    public int getBoardPosX() {
        return boardPosX;
    }

    /**
     * Estableix la posicio horizontal de la figura
     * @param boardPosX es la posicio horizontal de la figura
     */
    public void setBoardPosX(int boardPosX) {
        this.boardPosX = boardPosX;
    }

    /**
     * Retorna la posicio vertical de la figura
     * @return la posicio vertical de la figura
     */
    public int getBoardPosY() {
        return boardPosY;
    }

    /**
     * Estableix la posicio vertical de la figura
     * @param boardPosY es la posicio vertical de la figura
     */
    public void setBoardPosY(int boardPosY) {
        this.boardPosY = boardPosY;
    }

    /**
     * Retorna el limit esquerra de la figura
     * @return el limit esquerra de la figura
     */
    public int getColliderLeftCol() {
        return colliderLeftCol;
    }

    /**
     * Estableix el limit esquerra de la figura
     * @param colliderLeftCol es el limit esquerra de la figura
     */
    public void setColliderLeftCol(int colliderLeftCol) {
        this.colliderLeftCol = colliderLeftCol;
    }

    /**
     * Retorna el limit dret de la figura
     * @return el limit dret de la figura
     */
    public int getColliderRightCol() {
        return colliderRightCol;
    }

    /**
     * Estableix el limit dret de la figura
     * @param colliderRightCol  es el limit dret de la figura
     */
    public void setColliderRightCol(int colliderRightCol) {
        this.colliderRightCol = colliderRightCol;
    }

    /**
     * Retorna el limit inferior de la figura
     * @return el limit inferior de la figura
     */
    public int getColliderBottomRow() {
        return colliderBottomRow;
    }

    /**
     * Estableix el limit inferior de la figura
     * @param colliderBottomRow es el limit inferior de la figura
     */
    public void setColliderBottomRow(int colliderBottomRow) {
        this.colliderBottomRow = colliderBottomRow;
    }

    /**
     * Retorna el limit superior de la figura
     * @return el limit superior de la figura
     */
    public int getColliderTopRow() {
        return colliderTopRow;
    }

    /**
     * Estableix el limit superior de la figura
     * @param colliderTopRow es el limit superior de la figura
     */
    public void setColliderTopRow(int colliderTopRow) {
        this.colliderTopRow = colliderTopRow;
    }
}
