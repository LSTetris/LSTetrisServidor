package Model;

import java.io.Serializable;

/**
 * La classe Match implementa l'estuctura d'una partida i permet la seva serialització.
 */
public class Match implements Serializable{
    private int secondsPlayed;
    private int score;
    private int spectators;
    private char[][] board;
    private char[][] viewableBoard;
    private char[][] nextFigurePieces;
    private int nextFigureColor;
    private boolean running;
    private boolean lost;

    /**
     * Crea una nova Match
     */
    public Match(){}

    public Match(Match oldMatch){
        this.secondsPlayed = oldMatch.secondsPlayed;
        this.score = oldMatch.score;
        this.spectators = oldMatch.spectators;
        this.board = copyArray(oldMatch.board);
        this.viewableBoard = copyArray(oldMatch.viewableBoard);
        this.nextFigurePieces = copyArray(oldMatch.nextFigurePieces);
        this.nextFigureColor = oldMatch.nextFigureColor;
        this.running = oldMatch.running;
        this.lost = oldMatch.lost;
    }

    private char[][] copyArray(char[][] oldArray){
        char[][] newArray = new char[oldArray.length][oldArray[0].length];
        for(int y = 0; y < oldArray.length; y++){
            for(int x = 0; x < oldArray[0].length; x++){
                newArray[y][x] = oldArray[y][x];
            }
        }

        return newArray;
    }

    /**
     * Retorna els segons jugats de la partida
     * @return els segons jugats
     */
    public int getSecondsPlayed() {
        return secondsPlayed;
    }

    /**
     * Estableix els segons jugats de la partida
     * @param secondsPlayed son els  segons jugats
     */
    public void setSecondsPlayed(int secondsPlayed) {
        this.secondsPlayed = secondsPlayed;
    }

    /**
     * Retorna la puntuacio de la partida
     * @return la puntuacio
     */
    public int getScore() {
        return score;
    }

    /**
     * Estableix la puntuacio de la partida
     * @param score es la puntuacio
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Retorna l'estat actual del taulell
     * @return matriu de caracters que representa l'estat actual del taulell
     */
    public char[][] getBoard() {
        return board;
    }

    /**
     * Estableix l'estat actual del taulell
     * @param board es la matriu de caracters que representa l'estat actual del taulell
     */
    public void setBoard(char[][] board) {
        this.board = board;
    }

    /**
     * Comprova si el GameThread esta actiu
     * @return
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Estableix l'estat d'activitat del GameThread
     * @param running es true si es vol activar i fals si es vol desactivar
     */
    public void setRunning(boolean running) {
        this.running = running;
    }


    /**
     * Retorna les peces de la següent figura
     * @return matriu de caracters que representen les peces de la seguent figura
     */
    public char[][] getNextFigurePieces() {
        return nextFigurePieces;
    }

    /**
     * Retorna el color de la seguent figura
     * @return un enter que representa el color de la següent figura
     */
    public int getNextFigureColor() {
        return nextFigureColor;
    }

    /**
     * Estableix les peces de la següent figura
     * @param nextFigurePieces matriu de caracters que representen les peces de la seguent figura
     */
    public void setNextFigurePieces(char[][] nextFigurePieces) {
        this.nextFigurePieces = nextFigurePieces;
    }

    /**
     * Estableix el color de la seguent figura
     * @param nextFigureColor valor enter que representa el color de la seguent figura
     */
    public void setNextFigureColor(int nextFigureColor) {
        this.nextFigureColor = nextFigureColor;
    }

    /**
     * Comprova si la partida ha estat perduda
     * @return true si s'ha perdut i fals si no s'ha perdut encara
     */
    public boolean isLost() {
        return lost;
    }

    /**
     * Estableix si la partida ha estat perduda
     * @param lost es true si s'ha perdut i fals si no s'ha perdut encara
     */
    public void setLost(boolean lost) {
        this.lost = lost;
    }

    /**
     * Retorna el taulell observable
     * @return matriu de caracters que representa el taulell observable
     */
    public char[][] getViewableBoard() {
        return viewableBoard;
    }

    /**
     * Estableix el taulell observable
     * @param viewableBoard es la matriu de caracters que representa el taulell observable
     */
    public void setViewableBoard(char[][] viewableBoard) {
        this.viewableBoard = viewableBoard;
    }

    public int getSpectators() {
        return spectators;
    }

    public void setSpectators(int spectators) {
        this.spectators = spectators;
    }
}