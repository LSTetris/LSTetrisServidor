package Model;

import java.util.Random;

/**
 * La classe GameLogic gestiona la logica del joc del LSTetris
 */
public class GameLogic {
    //La partida
    private Match match;
    //La figura actual
    private Figure currentFigure;
    //La seguent figura
    private Figure upcomingFigure;
    //Totes les figures del joc
    private final char[][][][] figures = {
            //I
            {
                {   //Vertical
                    {'0', 'B', '0', '0'},
                    {'0', 'B', '0', '0'},
                    {'0', 'B', '0', '0'},
                    {'0', 'B', '0', '0'}
                },
                {   //Horitzontal
                    {'0', '0', '0', '0'},
                    {'B', 'B', 'B', 'B'},
                    {'0', '0', '0', '0'},
                    {'0', '0', '0', '0'}
                },
                {   //Vertical Girat
                    {'0', '0', 'B', '0'},
                    {'0', '0', 'B', '0'},
                    {'0', '0', 'B', '0'},
                    {'0', '0', 'B', '0'}
                },
                {   //Horitzontal Girat
                    {'0', '0', '0', '0'},
                    {'0', '0', '0', '0'},
                    {'B', 'B', 'B', 'B'},
                    {'0', '0', '0', '0'}
                }
            },

            //J
            {
                {   //Vertical
                    {'0', 'J', '0'},
                    {'0', 'J', '0'},
                    {'J', 'J', '0'}
                },
                {   //Vertical Girat
                    {'0', 'J', 'J'},
                    {'0', 'J', '0'},
                    {'0', 'J', '0'}
                },
                {   //Horitzontal
                    {'J', '0', '0'},
                    {'J', 'J', 'J'},
                    {'0', '0', '0'}
                },
                {   //Horitzontal Girat
                    {'0', '0', '0'},
                    {'J', 'J', 'J'},
                    {'0', '0', 'J'}
                }
            },

            //L
            {
                {   //Vertical
                    {'0', 'L', '0'},
                    {'0', 'L', '0'},
                    {'0', 'L', 'L'}
                },
                {   //Vertical Girat
                    {'L', 'L', '0'},
                    {'0', 'L', '0'},
                    {'0', 'L', '0'}
                },
                {   //Horitzontal
                    {'0', '0', 'L'},
                    {'L', 'L', 'L'},
                    {'0', '0', '0'}
                },
                {   //Horitzontal Girat
                    {'0', '0', '0'},
                    {'L', 'L', 'L'},
                    {'L', '0', '0'}
                }
            },

            //O
            {
                {
                    {'0', 'O', 'O', '0'},
                    {'0', 'O', 'O', '0'},
                    {'0', '0', '0', '0'}
                }
            },

            //S
            {
                {   //Vertical
                    {'S', '0', '0'},
                    {'S', 'S', '0'},
                    {'0', 'S', '0'}
                },
                {   //Horitzontal
                    {'0', '0', '0'},
                    {'0', 'S', 'S'},
                    {'S', 'S', '0'}
                },
                {   //Vertical Girat
                    {'0', 'S', '0'},
                    {'0', 'S', 'S'},
                    {'0', '0', 'S'}
                },
                {   //Horitzontal Girat
                    {'0', 'S', 'S'},
                    {'S', 'S', '0'},
                    {'0', '0', '0'}
                }
            },

            //T
            {
                {   //Vertical
                    {'0', 'T', '0'},
                    {'T', 'T', '0'},
                    {'0', 'T', '0'}
                },
                {   //Horitzontal
                    {'0', 'T', '0'},
                    {'T', 'T', 'T'},
                    {'0', '0', '0'}
                },
                {   //Vertical Girat
                    {'0', 'T', '0'},
                    {'0', 'T', 'T'},
                    {'0', 'T', '0'}
                },
                {   //Horitzontal Girat
                    {'0', '0', '0'},
                    {'T', 'T', 'T'},
                    {'0', 'T', '0'}
                }
            },

            //Z
            {
                {   //Vertical
                    {'0', '0', 'Z'},
                    {'0', 'Z', 'Z'},
                    {'0', 'Z', '0'}
                },
                {   //Horitzontal
                    {'0', '0', '0'},
                    {'Z', 'Z', '0'},
                    {'0', 'Z', 'Z'}
                },
                {   //Vertical Girat
                    {'0', 'Z', '0'},
                    {'Z', 'Z', '0'},
                    {'Z', '0', '0'}
                },
                {   //Horitzontal Girat
                    {'Z', 'Z', '0'},
                    {'0', 'Z', 'Z'},
                    {'0', '0', '0'}
                }
            }
    };

    /**
     * Crea una nova GameLogic a partir de una partida
     * @param match la partida que es jugara
     */
    public GameLogic(Match match){
        this.match = match;
    }


    /**
     * Rep la figura que es jugarà i la fa activa
     * @param f es la figura que es jugara i es fara activa
     */
    public void addNewFigure(Figure f){
        this.currentFigure = f;
    }

    /**
     * Afegeix la primera figura
     */
    public void addFirstFigure(){
        this.currentFigure = getNewFigure();
    }

    /**
     * Retorna una nova figura aleatoria.
     * @return una figura aleatoria de patró aleatori.
     */
    public Figure getNewFigure(){
        Random rand = new Random();

        /* rand.next(int) retorna un número de 0 a int-1. Si int fos 50 retornaria de 0-49.
        Si volem tot el rang hem de fer rand.next(50 + 1) -> 0-51;
         */

        //Triem quina figura
        int numFig = rand.nextInt(figures.length);
        //int numFig = 0;

        //Triem quina posició d'aquesta figura
        int posFig = rand.nextInt(figures[numFig].length);
        //int posFig = 1;

        //Busquem l'última posició on hi ha blocs per facilitar el càlcul de col·lisions
        int colliderTopRow = getColliderTop(figures[numFig][posFig]);
        int colliderBottomRow = getColliderBottom(figures[numFig][posFig]);
        int colliderLeftCol = getColliderLeft(figures[numFig][posFig]);
        int colliderRightCol = getColliderRight(figures[numFig][posFig]);

        //Retornem aquesta figura en una posició concreta, a les coordenades x = 4, y = 0
        return new Figure(figures[numFig][posFig], numFig, posFig, colliderBottomRow, colliderTopRow, colliderLeftCol, colliderRightCol, 3, 0);
    }

    /**
     * Retorna el limit superior d'una figura
     * @param chars es la figura
     * @return el limit superior de la figura
     */
    private int getColliderTop(char[][] chars) {
        int startColumnToCheck = chars.length;


        for(int y = chars.length - 1; y >= 0; y--){
            for(int x = 0; x < chars[y].length; x++){
                if(chars[y][x] != '0'){
                    if(y < startColumnToCheck){
                        startColumnToCheck = y;
                    }
                }
            }
        }

        return startColumnToCheck;
    }

    /**
     * Retorna el limit dret d'una figura
     * @param chars es la figura
     * @return el limit dret de la figura
     */
    private int getColliderRight(char[][] chars) {
        int endColumnToCheck = 0;

        //Busquem l'últim lloc on comprovar si hi ha un bloc
        for (int y = 0; y < chars.length; y++) {
            for (int x = chars[y].length - 1; x >= 0; x--) {
                if (chars[y][x] != '0') {
                    if(x > endColumnToCheck) {
                        endColumnToCheck = x;
                    }
                }
            }
        }

        return endColumnToCheck;

    }

    /**
     * Retorna el limit esquerre de una figura
     * @param chars es la figura
     * @return el limit esquerre de la figura
     */
    private int getColliderLeft(char[][] chars) {
        int startColumToCheck = chars[0].length;

        //Busquem l'últim lloc on comprovar si hi ha un bloc
        for (int y = 0; y < chars.length; y++) {
            for (int x = 0; x < chars[y].length; x++) {
                if (chars[y][x] != '0') {
                    if(x < startColumToCheck) {
                        startColumToCheck = x;
                    }
                }
            }
        }

        return startColumToCheck;
    }

    /**
     * Retorna el limit inferior de una figura
     * @param figure es la figura
     * @return es el limit inferior de la figura
     */
    private int getColliderBottom(char[][] figure) {
        int lowestRowToCheck = 0;

        //Busquem l'últim lloc on comprovar si hi ha un bloc
        for(int y = 0; y < figure.length; y++){
            for(int x = 0; x < figure[y].length; x++){
                if(figure[y][x] != '0'){
                    lowestRowToCheck = y;
                }
            }
        }

        return lowestRowToCheck;
    }

    /**
     * Reinicia la partida.
     */
    public void restartMatch(){
        char[][] newBoard = new char[22][10];

        for(int y = 0; y < newBoard.length; y++){
            for(int x = 0; x < newBoard[y].length; x++){
                newBoard[y][x] = '0';
            }
        }

        match.setBoard(newBoard);
        match.setScore(0);
        match.setSecondsPlayed(0);
        match.setRunning(true);
        match.setLost(false);

    }

    /**
     * Para la partida
     */
    public void stopMatch(){
        match.setRunning(false);
    }

    /**
     * Mira si a la dreta de la figura que s'està movent en l'espai del tauler col·lisiona amb algun altre bloc
     * @return true si hi ha col·lisió
     */
    public boolean collidesBottom(){
        boolean collides = false;

        int currFigPosY = currentFigure.getBoardPosY();
        int currFigColBot = currentFigure.getColliderBottomRow();

        int currBoardLen = this.match.getBoard().length;

        int realEndPos = currFigPosY + currFigColBot;

        //Si no ha arribat abaix
        if(currentFigure.getBoardPosY() + currentFigure.getColliderBottomRow() - currentFigure.getColliderTopRow() < this.match.getBoard().length - 1) {

            //Mirar si a la posició X de l'última línia on tenim hi ha blocs de la figura que movem està plena i
            //si en la mateixa X però en el tauler hi ha un bloc just a sota
            for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++) {
                for (int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++) {
                    if(currentFigure.getBoardPosY() == 19){
                    }
                    if (match.getBoard()[currentFigure.getBoardPosY() - currentFigure.getColliderTopRow() + y + 1][x + currentFigure.getBoardPosX()] != '0' &&
                            currentFigure.getPieces()[y][x] != '0') {
                        collides = true;
                    }
                }
            }
        } else {
            collides = true;
        }

        return collides;
    }

    /**
     * Mira si a l'esquerra de la figura que s'està movent en l'espai del tauler col·lisiona amb algun altre bloc
     * @return true si hi ha col·lisió
     */
    public boolean collidesLeft(){
        boolean collides = false;

        int currFigPosY = currentFigure.getBoardPosY();
        int currFigColLeft = currentFigure.getColliderLeftCol();

        //int currBoardLen = this.match.getBoard().length;

        //int realEndPos = currFigPosY + currFigColBot;

        //Si no ha arribat abaix
        if(currentFigure.getBoardPosY() + currentFigure.getColliderBottomRow() - currentFigure.getColliderTopRow() < this.match.getBoard().length - 1) {

            //Mirar si a la posició X de l'última línia on tenim hi ha blocs de la figura que movem està plena i
            //si en la mateixa X però en el tauler hi ha un bloc just a sota
            for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++) {
                for (int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++) {
                    if(currentFigure.getBoardPosY() == 19){
                        System.out.println("A punt d'explotar");
                    }
                    if (match.getBoard()[currentFigure.getBoardPosY() - currentFigure.getColliderTopRow() + y][x + currentFigure.getBoardPosX() - 1] != '0' &&
                            currentFigure.getPieces()[y][x] != '0') {
                        collides = true;
                    }
                }
            }
        } else {
            collides = true;
        }

        return collides;
    }

    /**
     * Mira si a sota de la figura que s'està movent en l'espai del tauler col·lisiona amb algun altre bloc
     * @return true si hi ha col·lisió
     */
    public boolean collidesRight(){
        boolean collides = false;

        int currFigPosY = currentFigure.getBoardPosY();
        int currFigColRight = currentFigure.getColliderRightCol();

        //int currBoardLen = this.match.getBoard().length;

        //int realEndPos = currFigPosY + currFigColBot;

        //Mirar si a la posició X de l'última línia on tenim hi ha blocs de la figura que movem està plena i
        //si en la mateixa X però en el tauler hi ha un bloc just a sota
        for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++) {
            for (int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++) {
                if (match.getBoard()[currentFigure.getBoardPosY() - currentFigure.getColliderTopRow() + y][x + currentFigure.getBoardPosX() + 1] != '0' &&
                        currentFigure.getPieces()[y][x] != '0') {
                    collides = true;
                }
            }
        }


        return collides;
    }

    /**
     * Mou la peça que està activa cap aball
     */
    public void moveFigureDown(){
        if(currentFigure.getBoardPosY() == 20){
        }
        if(currentFigure.getBoardPosY() - currentFigure.getColliderTopRow() + 1 < 21 && !collidesBottom()) {
            currentFigure.setBoardPosY(currentFigure.getBoardPosY() + 1);
        }
    }

    /**
     * Rep una figura nova i la guarda com a la pròxima figura
     * @param newFigure serà la pròxima figura a ser jugada
     */
    public void addUpcomingFigure(Figure newFigure) {
        this.upcomingFigure = newFigure;
    }

    /**
     * Retorna la seguent figura
     * @return
     */
    public Figure getUpcomingFigure(){
        return this.upcomingFigure;
    }

    /**
     * Retorna la figura actual rotada 90 graus cap a l'esquerra (sentit antihorari)
     * @return la figura rotada
     */
    public Figure getFigureRotateLeft(){
        int nextPos = currentFigure.getNumPosition();

        if(nextPos - 1 < 0){
            nextPos = figures[currentFigure.getNumFigure()].length - 1;
        } else {
            nextPos -= 1;
        }

        return new Figure(figures[currentFigure.getNumFigure()][nextPos],
                currentFigure.getNumFigure(),
                nextPos,
                getColliderBottom(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderTop(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderLeft(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderRight(figures[currentFigure.getNumFigure()][nextPos]),
                currentFigure.getBoardPosX(),
                currentFigure.getBoardPosY());

    }

    /**
     * Retorna la figura actual rotada 90 graus cap a la dreta (sentit horari)
     * @return la figura rotada
     */
    public Figure getFigureRotateRight(){
        int nextPos = currentFigure.getNumPosition();

        if(nextPos + 1 == figures[currentFigure.getNumFigure()].length){
            nextPos = 0;
        } else {
            nextPos += 1;
        }

        return new Figure(figures[currentFigure.getNumFigure()][nextPos],
                currentFigure.getNumFigure(),
                nextPos,
                getColliderBottom(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderTop(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderLeft(figures[currentFigure.getNumFigure()][nextPos]),
                getColliderRight(figures[currentFigure.getNumFigure()][nextPos]),
                currentFigure.getBoardPosX(),
                currentFigure.getBoardPosY());

    }

    /**
     * Retorna una copia del taulell
     * @return matriu de caracters que representa el taulell
     */
    public char[][] cloneBoard(){

        int ySize = this.match.getBoard().length;
        int xSize = this.match.getBoard()[0].length;

        char[][] clonedBoard = new char[ySize][xSize];


        for(int y = 0; y < ySize; y++ ) {
            for(int x = 0; x < xSize; x++ ) {
                clonedBoard[y][x] = this.match.getBoard()[y][x];
            }
        }

        return clonedBoard;
    }

    /**
     * Registra les posicions de la figura
     */
    public void translateFigureToBoard() {
        char[][] updatedBoard = this.match.getBoard();

        for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++){
            for(int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++){
                if(currentFigure.getPieces()[y][x] != '0'){
                    updatedBoard[y - currentFigure.getColliderTopRow() + currentFigure.getBoardPosY()][x + currentFigure.getBoardPosX()] =
                            currentFigure.getPieces()[y][x];
                }
            }
        }

        this.match.setBoard(updatedBoard);

    }


    /**
     * Registra les posicions de la figura en la vista
     */
    public void translateFigureToViewableBoard() {
        //S'està instanciant! S'ha de clonar!!!
        char[][] updatedBoard = cloneBoard();

        for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++){
            for(int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++){
                if(currentFigure.getPieces()[y][x] != '0'){
                    updatedBoard[y - currentFigure.getColliderTopRow() + currentFigure.getBoardPosY()][x + currentFigure.getBoardPosX()] =
                            currentFigure.getPieces()[y][x];
                }
            }
        }

        this.match.setViewableBoard(updatedBoard);
        //pintaPartida(updatedBoard);

    }

    /**
     * Imprimeix el taulell per linea de comandes
     * @param newBoard una matriu de caracters que representa el taulell
     */
    public void pintaPartida(char[][] newBoard){

        for(int y = 0; y < newBoard.length; y++){
            for(int x = 0; x < newBoard[y].length; x++){
                System.out.print(newBoard[y][x]);
            }
            System.out.println("");
        }
        System.out.println("-------");
    }

    /**
     * Retorna si la fitxa es el top.
     * @return true si la fitxa es el top i false si no.
     */
    public boolean isTop() {
        boolean isTop = false;

        for(int y = currentFigure.getColliderTopRow(); y <= currentFigure.getColliderBottomRow(); y++){
            for(int x = currentFigure.getColliderLeftCol(); x <= currentFigure.getColliderRightCol(); x++) {
                if (this.match.getBoard()[y - currentFigure.getColliderTopRow()][x + currentFigure.getBoardPosX()] != '0'){
                    isTop = true;
                }
            }
        }

        return isTop;
    }

    /**
     * Mou la figura actual cap a la dreta
     */
    public void moveFigureRight(){
        if(this.currentFigure.getBoardPosX() + this.currentFigure.getColliderRightCol() < this.match.getBoard()[0].length - 1 && !collidesRight()) {
            this.currentFigure.setBoardPosX(this.currentFigure.getBoardPosX() + 1);
        }
    }

    /**
     * Mou la figura actual cap a l'esquerra
     */
    public void moveFigureLeft(){
        int boardPosX = this.currentFigure.getBoardPosX();
        int boardColliderLeftCol = this.currentFigure.getColliderLeftCol();

        if(this.currentFigure.getBoardPosX() + this.currentFigure.getColliderLeftCol() >= 1 && !collidesLeft()) {
            this.currentFigure.setBoardPosX(this.currentFigure.getBoardPosX() - 1);
        }
    }

    /**
     * Comprova si la figura pot ser girada 90 graus cap a l'esquerra (sentit antihorari)
     * @return true si pot ser girada sense invadir cap espai ocupat i fals sino
     */
    public boolean canTurnFigureLeft(){
        Figure nextFigureTurnLeft = getFigureRotateLeft();
        boolean canTurnLeft = true;

        if(currentFigure.getBoardPosY() + nextFigureTurnLeft.getColliderBottomRow() < match.getBoard().length
                && currentFigure.getBoardPosY() - nextFigureTurnLeft.getColliderTopRow() >= 0
                && currentFigure.getBoardPosX() + nextFigureTurnLeft.getColliderRightCol() < match.getBoard()[0].length
                && currentFigure.getBoardPosX() - nextFigureTurnLeft.getColliderLeftCol() >= 0) {
            for (int y = nextFigureTurnLeft.getColliderTopRow(); y <= nextFigureTurnLeft.getColliderBottomRow(); y++) {
                for (int x = nextFigureTurnLeft.getColliderLeftCol(); x <= nextFigureTurnLeft.getColliderRightCol(); x++) {
                    if (match.getBoard()[currentFigure.getBoardPosY() + y][currentFigure.getBoardPosX() + x] != '0' &&
                            nextFigureTurnLeft.getPieces()[y][x] != '0') {
                        canTurnLeft = false;
                    }
                }
            }
        } else {
            canTurnLeft = false;
        }


        return canTurnLeft;
    }


    /**
     * Comprova si la figura pot ser girada 90 graus cap a la dreta (sentit horari)
     * @return true si pot ser girada sense invadir cap espai ocupat i fals sino
     */
    public boolean canTurnFigureRight(){
        Figure nextFigureTurnLeft = getFigureRotateRight();
        boolean canTurnLeft = true;

        if(currentFigure.getBoardPosY() + nextFigureTurnLeft.getColliderBottomRow() < match.getBoard().length
                && currentFigure.getBoardPosY() - nextFigureTurnLeft.getColliderTopRow() >= 0
                && currentFigure.getBoardPosX() + nextFigureTurnLeft.getColliderRightCol() < match.getBoard()[0].length
                && currentFigure.getBoardPosX() - nextFigureTurnLeft.getColliderLeftCol() >= 0) {
            for(int y = nextFigureTurnLeft.getColliderTopRow(); y <= nextFigureTurnLeft.getColliderBottomRow(); y++){
                for(int x = nextFigureTurnLeft.getColliderLeftCol(); x <= nextFigureTurnLeft.getColliderRightCol(); x++){
                    if(match.getBoard()[currentFigure.getBoardPosY() + y][currentFigure.getBoardPosX() + x] != '0' &&
                            nextFigureTurnLeft.getPieces()[y][x] != '0'){
                        canTurnLeft = false;
                    }
                }
            }
        } else {
            canTurnLeft = false;
        }

        return canTurnLeft;
    }

    /**
     * Gira la figura actual 90 graus cap a l'esquerra (sentit antihorari).
     */
    public void turnFigureLeft(){
        if(canTurnFigureLeft()){
            currentFigure = getFigureRotateLeft();
        }
    }


    /**
     * Gira la figura actual 90 graus cap a la dreta (sentit horari).
     */
    public void turnFigureRight(){
        if(canTurnFigureRight()){
            currentFigure = getFigureRotateRight();
        }
    }

    /**
     * Retorna la partida que s'esta jugant.
     * @return la partida
     */
    public Match returnMatch(){
        return this.match;
    }

    /**
     * Esborra les files de baix del taulell que ja estan omplertes completament.
     */
    public void removeFilledRows() {
        char[][] board = match.getBoard();
        boolean removeRow;

        for(int y = board.length - 1; y >= 0; y--){
            removeRow = true;
            for(int x = 0; x < board[0].length && removeRow; x++){
                if(board[y][x] == '0'){
                    removeRow = false;
                }
            }

            if(removeRow) {
                addScore(100);
                for (int k = y; k > 0; k--){
                    for (int x = 0; x < board[0].length; x++) {
                        board[k][x] = board[k - 1][x];
                    }
                }
                for (int x = 0; x < board[0].length; x++) {
                    board[0][x] = board[0][x] = '0';
                }
                y += 1;
            }
        }
    }

    public void addScore(int scoreToAdd) {
        match.setScore(match.getScore() + scoreToAdd);
    }

    public void endMatch() {
        match.setRunning(false);
    }
}
