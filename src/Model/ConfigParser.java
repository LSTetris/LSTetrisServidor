package Model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *ConfigParser es la classe que s'encarrega de llegir la configuració de un json i retornar-la al model.
 */
public class ConfigParser {
    //El port de connexio a la base de dades
    private int dbPort;
    //La ip de la base de dades
    private String dbIp;
    //El nom de la base de dades
    private String dbName;
    //L'usuari de la base de dades
    private String dbUser;
    //La contrasenya de la base de dades
    private String dbPassword;
    //El port de connexió amb el client
    private int clientPort;


    /**
     * Llegeix el fitxer config.json i carrega els atributs
     */
    public ConfigParser() {
        FileReader f;
        JsonObject jsonObject = null;
        JsonParser parser = new JsonParser();

        try {
            f = new FileReader(new File("config.json"));
            jsonObject = (JsonObject) parser.parse(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        this.dbPort = jsonObject.get("db_port").getAsInt();
        this.dbIp = jsonObject.get("db_ip").getAsString();
        this.dbName = jsonObject.get("db_name").getAsString();
        this.dbUser = jsonObject.get("db_user").getAsString();
        this.dbPassword = jsonObject.get("db_password").getAsString();
        this.clientPort = jsonObject.get("client_port").getAsInt();
    }

    /**
     * Retorna el port de la base de dades
     * @return es el port de la pase de dades
     */
    public int getDbPort() {
        return dbPort;
    }

    /**
     * Retorna l'ip de la base de dades
     * @return es l'ip de la base de dades
     */
    public String getDbIp() {
        return dbIp;
    }

    /**
     * Retorna el nom de la base de dades
     * @return es el nom de la base de dades
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Retorna l'usuari de la base de dades
     * @return es l'usuari de la base de dades
     */
    public String getDbUser() {
        return dbUser;
    }

    /**
     * Retorna la contrasenya de la base de dades
     * @return es la contrasenya.
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * Retorna el port de connexió amb el client
     * @return es el port de connexió amb el client
     */
    public int getClientPort() {
        return clientPort;
    }
}
