package Model;

/**
 * La classe abstracte color defineix constants pels color utilitzats en el joc del tetris.
 */
public abstract class Color {
    // El color del fons
    public final static char background = 'B';
    //El color de la figura 1
    public final static char figure1 = 'A';
    //El color de la figura 2
    public final static char figure2 = 'C';
    //El color de la figura 3
    public final static char figure3 = 'D';
    //El color de la figura 4
    public final static char figure4 = 'E';
    //El color de la figura 5
    public final static char figure5 = 'F';
    //El color de la figura 6
    public final static char figure6 = 'G';
    //El color de la figura 7
    public final static char figure7 = 'H';

}
