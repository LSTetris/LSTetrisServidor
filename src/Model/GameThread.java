package Model;

import Database.DatabaseManager;
import Network.DedicatedServer;

import java.awt.event.KeyEvent;
import java.lang.reflect.Array;
import java.util.ArrayList;

import java.util.ArrayList;

/**
 * La classe GameThread gestiona el fil d'execucio del joc de tal forma que es puguin executar diversos simultaneament.
 */
public class GameThread extends Thread {

    private GameLogic gameLogic;
    private Match match;
    private boolean gameRunning = false;
    private boolean gamePaused = false;
    private int spectators = 0;
    private DedicatedServer dedicatedServer;
    private DatabaseManager databaseManager;
    private ArrayList<String> tecles;
    private String userName;
    private ArrayList<DedicatedServer> dedicatedServersSpectators;
    private ArrayList<Match> partidaGuardada;
    private int maxSpectators;

    /**
     * Crea un nou GameThread
     * @param dedicatedServer es el servidor dedicat que utilitzara
     * @param match es la partida que es jugara
     * @param databaseManager es el gestor de base de dades que s'emprara
     * @param spectators són els dedicatedServers que espectegen la partida
     */
    public GameThread(DedicatedServer dedicatedServer, Match match, DatabaseManager databaseManager, ArrayList<DedicatedServer> spectators){
        this.dedicatedServer = dedicatedServer;
        this.match = match;
        gameRunning = false;
        gameLogic = new GameLogic(this.match);
        this.databaseManager = databaseManager;
        this.dedicatedServersSpectators = spectators;
        this.partidaGuardada = new ArrayList<>();
    }

    @Override
    public void run(){

        int elapsedTime = 0;

        //Mirem quines són les seves tecles
        try {
            tecles = databaseManager.query("SELECT ascii_move_right, ascii_move_left, ascii_move_down, ascii_turn_right, ascii_turn_left, ascii_pause FROM Jugador WHERE nickname = '" + userName + "';").get(0);
            //System.out.println("SELECT ascii_move_right, ascii_move_left, ascii_move_down, ascii_turn_right, ascii_turn_left, ascii_pause FROM Jugador WHERE nickname = '" + userName + "';");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Fem que el joc comenci
        gameRunning = true;
        gameLogic.restartMatch();
        gameLogic.addUpcomingFigure(gameLogic.getNewFigure());
        gameLogic.addFirstFigure();
        match.setNextFigurePieces(gameLogic.getUpcomingFigure().getPieces());
        match.setNextFigureColor(gameLogic.getUpcomingFigure().getNumFigure());
        
        maxSpectators = 0;

        partidaGuardada.clear();

        while(gameRunning){
            try {
                //Esperem 60 milisegons
                Thread.sleep(60);
                elapsedTime += 60;

                //Si el joc està funcionant
                if (!match.isLost()) {
                    //Lògica del joc
                    if(match.isRunning()) {
                        match.setSecondsPlayed(match.getSecondsPlayed() + 60);
                    }

                    //Si ha passat 1200 milisegons ja podem moure la peça cap abaix
                    if (elapsedTime >= 1200) {
                        elapsedTime = 0;

                        if (match.isRunning()) {
                            //Si no hi ha col·lisió per sota de la figura
                            if (!gameLogic.collidesBottom()) {
                                gameLogic.moveFigureDown();

                                //En cas que sí hi hagi col·lisió
                            } else {
                                //Si ha arribat a dalt de tot i per tant ha perdut
                                if (gameLogic.isTop()) {
                                    match.setLost(true);
                                    gameRunning = false;

                                    //Si encara pot continuar jugant
                                } else {

                                    gameLogic.addScore(25);

                                    //Posar la figura que s'està movent al tauler
                                    gameLogic.translateFigureToBoard();

                                    //Esborrem totes les files que estiguin plenes
                                    gameLogic.removeFilledRows();

                                    //Seguidament fem activa la figura que hi havia esperant
                                    //match.setNextFigurePieces(gameLogic.getUpcomingFigure().getPieces());
                                    //match.setNextFigureColor(gameLogic.getUpcomingFigure().getNumFigure());
                                    gameLogic.addNewFigure(gameLogic.getUpcomingFigure());

                                    //Fem una nova figura que serà la següent de l'activa
                                    gameLogic.addUpcomingFigure(gameLogic.getNewFigure());

                                    if (gameLogic.isTop()) {
                                        System.out.println(gameLogic.isTop());
                                        match.setLost(true);
                                        gameRunning = false;
                                        dedicatedServer.sendObject(new String[]{"Game", "End"}, this.match);
                                    }
                                }
                            }
                        }
                    }
                }

                //Seguidament fem activa la figura que hi havia esperant
                match.setNextFigurePieces(gameLogic.getUpcomingFigure().getPieces());
                match.setNextFigureColor(gameLogic.getUpcomingFigure().getNumFigure());

                //Actualitzem les dades per enviar al client
                gameLogic.translateFigureToViewableBoard();

                //Enviem les dades al client
                dedicatedServer.sendObject(new String[]{"Game", "Status"}, this.match);
                Match toSave = new Match(this.match);
                partidaGuardada.add(toSave);
                for (DedicatedServer ds : dedicatedServersSpectators) {
                    ds.sendObject(new String[]{"Spectator", "Play"}, this.match);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Comprova si una clau concreta ha sigut premuda
     * @param key la clau concreta
     */
    public void keyPressed(String key){
        if(this.gameRunning){
            if(!match.isLost() && match.isRunning()) {
                if (key.equals(tecles.get(1))) {
                    this.gameLogic.moveFigureLeft();
                } else if (key.equals(tecles.get(0))) {
                    this.gameLogic.moveFigureRight();
                } else if (key.equals(tecles.get(2))) {
                    this.gameLogic.moveFigureDown();
                } else if (key.equals(tecles.get(3))) {
                    this.gameLogic.turnFigureRight();
                } else if (key.equals(tecles.get(4))) {
                    this.gameLogic.turnFigureLeft();
                }
            }

            if(key.equals(tecles.get(5))){
                this.pauseMatch();
            }
        }
    }

    /**
     * Para el fil d'execucio de la partida.
     */
    public void stopGameThread() {
        //Acabar la partida
        gameRunning = false;
    }

    /**
     * Registra l'usuari per que pugui jugar
     * @param userName es el nom d'usuari
     */
    public void registerUser(String userName) {
        this.userName = userName;
    }

    /**
     * Augmenta en 1 el nombre d'espectadors
     */
    public void addSpectators(){
        match.setSpectators(match.getSpectators() + 1);
        maxSpectators++;
    }

    /**
     * Disminueix en 1 el nombre d'espectadors
     */
    public void removeSpectator(){
        match.setSpectators(match.getSpectators() - 1);
    }

    public int getSpectators() {
        return spectators;
    }

    /**
     * Calcula el temps que ha transcorregut des de l'inici de la partida
     * @return el temps transcorregut
     */
    public int calculateTime(){
        return match.getSecondsPlayed();
    }

    /**
     * Mètode que pausa la partida
     */
    public void pauseMatch() {
        this.match.setRunning(!this.match.isRunning());
    }

    /**
     * Mètode que reinicia la lògica de la partida
     */
    public void restartMatch() {
        this.gameLogic.restartMatch();
    }

    /**
     * Acaba la partida
     */
    public void endMatch() {
        this.gameLogic.endMatch();
    }

    public boolean isRunning(){
        return this.match.isRunning();
    }

    public ArrayList<Match> getPartidaGuardada() {
        return this.partidaGuardada;
    }

    public int getMaxSpectators() {
        return maxSpectators;
    }
}
