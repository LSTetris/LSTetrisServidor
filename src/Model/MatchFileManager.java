package Model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

/**
 * Classe que gestiona els fitxers del directori matches. Amb això es carrega els fitxers que hi ha i permet
 * esborrar-los
 */
public class MatchFileManager {

    private Vector<String> filenames;
    private String activeUserDirectory;

    /**
     * Constructor de la classe que llegeix tots els arxius que hi ha a dins d'un directori.
     */
    public MatchFileManager(){
        File f = new File("matches");
        filenames = new Vector<String>(Arrays.asList(f.list()));

        //Eliminem el fitxer .gitkeep per evitar pujar-lo a internet
        filenames.remove(".gitkeep");
        activeUserDirectory = "";
    }

    /**
     * Mètode que carrega els arxius que hi ha a dins del directori d'un usuari
     * @param user String amb el nom d'usuari que es vulgui veure les partides
     */
    public void loadUserMatches(String user){
        if(user != null) {
            activeUserDirectory = user;
            File f = new File("matches" + File.separator + user);
            filenames = new Vector<String>(Arrays.asList(f.list()));
        }
    }

    /**
     * Mètode que llegeix tots els directoris que hi ha a la root de partides guardades (matches).
     */
    public void loadUsers(){
        activeUserDirectory = "";
        File f = new File("matches");
        filenames = new Vector<String>(Arrays.asList(f.list()));
        filenames.remove(".gitkeep");
    }

    public String getActiveUserDirectory() {
        return activeUserDirectory;
    }

    public Vector<String> getFilenames() {
        return filenames;
    }

    /**
     * Mètode que elimina un arxiu a partir del seu nom
     * @param name String amb el nom de l'arxiu que es vol esborrar
     * @throws IOException Exception que salta si no es troba l'arxiu
     */
    public void deleteFile(String name) throws IOException{

        Files.delete(Paths.get("matches" + File.separator + activeUserDirectory + File.separator + name));
        loadUserMatches(activeUserDirectory);

    }

    /**
     * Mètode que serveix per retornar les partides d'un usuari específic quan un client fa la petició
     * @param user String amb el nom de l'usuari
     * @return ArrayList<String> amb els noms de les partides
     */
    public ArrayList<String> returnUserMatches(String user){
        if(user != null) {

            File f = new File("matches" + File.separator + user);
            return new ArrayList<>(Arrays.asList(f.list()));
        }
        return new ArrayList<>();
    }

}
