package Model;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe que servirà per gestionar els usuaris i afegirlos a la BBDD
 */
public class UserManager {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Valida els parametres d'un usuari i genera un string amb el resultat
     * @param u es l'usuari a evaluar
     * @return un string que conté els errors que s'han trobat en l'usuari. Si l'usuari es correcte retornara un String buit.
     */
    public static String validateRegisterParameters(User u) {
        String sb = comprovarUsuari(u.getNickname()) +
                comprovarEmail(u.getEmail()) +
                comprovarPassword(u.getPassword());
        return sb;
    }

    /**
     * Comprova que el camp del nickname tingui informacio.
     * @param user es el nickname de l'usuari
     * @return "" si te informacio i "Camp d'usuari buit\n" si no hi ha text.
     */
    private static String comprovarUsuari(String user){
        if (user.isEmpty()){
            return "Camp d'usuari buit\n";
        }
        return "";
    }

    /**
     * Comprova que el camp del mail no estigui buit i coincideixi amb una adreça valida
     * @param mail es el mail de l'usuari
     * @return un string indicant l'error que s'ha produit. Si no s'ha produit cap error estarà buit.
     */
    private static String comprovarEmail(String mail) {
        if (mail.isEmpty()){
            return "Camp de correu buit\n";
        } else {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(mail);
            if (!matcher.find()){
                return "Correu amb format incorrecte\n";
            }

        }
        return "";
    }

    /**
     * Comprova que la contrasenya d'un usuari sigui correcte
     * @param password es la contrasenya de l'usuari
     * @return un string amb els errors que s'han donat. Si no s'ha produit cap error sera buit.
     */
    private static String comprovarPassword(String password){
        StringBuilder sb = new StringBuilder();
        if (password.isEmpty()) {
            return "Camp de contrasenya buit\n";
        } else {
            if (password.equals(password.toLowerCase()) || password.equals(password.toUpperCase())) {
                sb.append("El password ha de contenir majúscules i minúscules\n");
            }
            if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")) {
                sb.append("El password ha de contenir números i lletres\n");
            }
        }
        return sb.toString();
    }
}