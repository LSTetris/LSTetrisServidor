import Controller.ViewWindowController;
import Controller.WindowController;
import Database.DatabaseManager;
import Model.ConfigParser;
import Model.MatchFileManager;
import Model.StatisticSet;
import Model.UserManager;
import Network.NetworkManager;
import View.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class LSTetrisServer {

    public static void main(String[] args) {

        //Instanciem la classe ConfigParser, que llegeix el JSON
        ConfigParser config = new ConfigParser();

        /* S'invoca Swing*/
        SwingUtilities.invokeLater(() -> {
            // Crear les vistes
            MainView mainView = new MainView();
            AdmUsrView admView = new AdmUsrView();
            GraphView graphView = new GraphView(new StatisticSet(), new StatisticSet());

            // Crear el model
            UserManager model = new UserManager();
            MatchFileManager fileManager = new MatchFileManager();

            //Creem la vista del fitxers
            MatchFileManagerView matchFileManagerView = new MatchFileManagerView(fileManager.getFilenames());

            //Crear el controlador de BBDD
            DatabaseManager databaseManager = new DatabaseManager(
                    config.getDbUser(),
                    config.getDbPassword(),
                    config.getDbName(),
                    config.getDbIp(),
                    config.getDbPort());

            // Crear el networking
            NetworkManager server = new NetworkManager(config.getClientPort(), databaseManager);

            // Crear el controlador i enllaçar C->V i C->M
            WindowController controller = new WindowController(server, databaseManager, mainView, admView, graphView, fileManager, matchFileManagerView);
            ViewWindowController viewController = new ViewWindowController(server, databaseManager);

            // Fer la relació V->C i S->C
            mainView.registerController(controller, viewController);
            admView.registerController(controller);
            server.registerController(controller);
            matchFileManagerView.registerController(controller);


            //Iniciem el servidor
            server.start();

            // Fer les vistes
            mainView.setVisible(true);
            admView.setVisible(false);
            graphView.setVisible(false);
        });
    }
}
