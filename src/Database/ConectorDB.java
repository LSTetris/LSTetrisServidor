package Database;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * La classe ConectorDB permet la comunicació entre programes Java i una base de dades en MySQL.
 * @author C1
 */
public class ConectorDB {
    private String userName;
    private String password;
    private String db;
    private int port;
    private String url;
    private Connection conn = null;
    private Statement s;

    /**
     * Crea un conector nou amb un user, un password, una base de dades existent i un port de conexió.
     * @param usr Es el nom de l'usuari de la base de dades.
     * @param pass Es la contrasenya de l'usuari de la base de dades.
     * @param db Es el nom exacte de la base de dades que es vol utilitzar.
     * @param port Es el port que es desitja utilitzar per conectar-se a la base de dades.
     */
    public ConectorDB(String usr, String pass, String db, String ip, int port) {
        this.userName = usr;
        this.password = pass;
        this.db = db;
        this.port = port;
        this.url = "jdbc:mysql://" + ip;
        this.url += ":" + port + "/";
        this.url += db + "?autoReconnect=true&useSSL=true&verifyServerCertificate=false" ;
    }

    /**
     * Conecta amb la base de dades de MySQL.
     * @return Cert si s'ha pogut establir connexió.
     */
    public boolean connect() {
        try {
            //Inicialitzem la classe Connection de jdbc
            Class.forName("com.mysql.jdbc.Connection");
            //Provem de conectar.
            conn = (Connection) DriverManager.getConnection(url, userName, password);
            return true;
        } catch (SQLException ex) {
            System.out.println("Problema al connectar-nos a la base de dades " + url + ".");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return false;

    }

    /**
     * Crea una query SQL de tipus insert a partir de un snippet de codi SQL i l'executa. Aquesta funció podria
     * funcionar també amb altres tipus de commandes, però és recomenable cenyir-se als inserts per evitar errors. La
     * funcio imprimirà per consola l'error si no s'ha pogut realitzar l'operació.
     * @param query Es el snippet SQL que es vol executar.
     */
    public void insertQuery(String query){
        try {
            s =(Statement) conn.createStatement();
            s.executeUpdate(query);

        } catch (SQLException ex) {
            System.out.println("Problema al Inserir. " + ex.getSQLState());
            System.out.println(ex.getErrorCode()+ ex.getMessage());
        }
    }

    /**
     * Crea una query SQL de tipus update a partir de un snippet de codi SQL i l'executa. Aquesta funció podria
     * funcionar també amb altres tipus de commandes, però és recomenable cenyir-se als updates per evitar errors. La
     * funcio imprimirà per consola l'error si no s'ha pogut realitzar l'operació.
     * @param query Es el snippet SQL que es vol executar.
     */
    public void updateQuery(String query){
    	 try {
             s =(Statement) conn.createStatement();
             s.executeUpdate(query);

         } catch (SQLException ex) {
             System.out.println("Problema al Modificar. " + ex.getSQLState());
             System.out.println(ex.getMessage());
         }
    }

    /**
     * Crea una query SQL de tipus delete a partir de un snippet de codi SQL i l'executa. Aquesta funció podria
     * funcionar també amb altres tipus de commandes, però és recomenable cenyir-se als deletes per evitar errors. La
     * funcio imprimirà per consola l'error si no s'ha pogut realitzar l'operació.
     * @param query Es el snippet SQL que es vol executar.
     */
    public void deleteQuery(String query){
    	 try {
             s =(Statement) conn.createStatement();
             s.executeUpdate(query);
             
         } catch (SQLException ex) {
             System.out.println("Problema al Eliminar." + ex.getSQLState());
         }
    	
    }

    /**
     * Consegueix resultats de la base de dades en un result set a partir de un snippet SQL. La funció imprimirà per
     * consola l'error si no s'ha pogut realitzar la selecció de dades.
     * @param query És l'snippet SQL que conté el select.
     * @return el resultat de la quèrie, en forma de ResultSet.
     */
    public ResultSet selectQuery(String query){
    	ResultSet rs = null;
    	 try {
             s =(Statement) conn.createStatement();
             rs = s.executeQuery (query);
             
         } catch (SQLException ex) {
             System.out.println("Problema al Recuperar les dades. " + ex.getSQLState());
         }
		return rs;
    }

    /**
     * Es desconnecta de la base de dades MySQL.
     */
    public void disconnect(){
    	try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Problema al tancar la connexió --> " + e.getSQLState());
		}
    }

}
