package Database;

import Model.StatisticSet;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;


/**
 * La classe DatabaseManager permet la aplicació de queries en una base de dades en MySQL des de Java.
 * @author C1
 * @version 1.0
 * @since 2018-05-20
 */
public class DatabaseManager {

    private final String JUGADOR_COLUMN_NAMES = "nickname, correu, contrasenya, estat_connectat, data_registre, ultim_login, num_partides, punts, ascii_move_left, ascii_move_right, ascii_move_down, ascii_turn_left, ascii_turn_right, ascii_pause";
    private final String PARTIDA_COLUMN_NAMES = "id_jugador, punts, file_path, data_inici, pic_maxim_espectadors";
    private boolean connected;
    private ConectorDB conn;

    /**
     * Crea un nou gestor de connexions amb bases de dades.
     */
    public DatabaseManager(String user, String password, String dbName, String ip, int port) {
        this.connected = false;
        conn = new ConectorDB(user, password, dbName, ip, port);
        connected = conn.connect();
    }

    /**
     * Es desconnecta de la base de dades de MySQL
     */
    public void disconnect () {
        conn.disconnect();
        connected = false;
    }

    /**
     * Obté els resultats d'una querie com a arrayList d'arraylists de string. Cada arrayList contenedor té una fila de
     * la querie.
     * @param query Es l'snippet de SQL que s'utilitzarà per realitzar la querie.
     * @return L'arraylist d'arraylists que representa la taula resultant, amb cada arrayList contenedor sent una fila.
     */
    public ArrayList<ArrayList<String>> query(String query) throws Exception {

        try {

            ResultSet rs = conn.selectQuery(query);

            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            ArrayList rows = new ArrayList();

            while (rs.next()) {
                ArrayList row = new ArrayList();

                for (int i = 1; i <= colCount; i++) {
                    row.add(rs.getString(i));
                    //System.out.println(rs.getString(i));
                }
                //System.out.println("-------");
                rows.add(row);
            }
            return rows;

        } catch (SQLException e1) {
            System.out.println("Error a MySQL");
            System.out.println(e1.getMessage());
            throw e1;

        } catch (NullPointerException e2) {
            System.out.println("La quèrie no ha retornat resultats");
            System.out.println(e2.getMessage());
            throw e2;
        }

    }

    /**
     * Afegeix un usuari a la base de dades.
     * @param nickname Es el nickname de l'usuari.
     * @param correu Es el correu de l'usuari.
     * @param password Es la contrasenya de l'usuari.
     */
    public void createUser (String nickname, String correu, String password) {
        conn.insertQuery("INSERT INTO Jugador ("+JUGADOR_COLUMN_NAMES+") VALUES ('" + nickname + "','" + correu +"','"+password+"',"+ true + ", CURDATE(), NOW(), 0, 0, 'A', 'D', 'S', 'Q', 'E', 'P');");
    }

    /**
     * Afegeix una partida a la base de dades.
     * @param id_jugador Es el id del jugador que ha realitzat la partida.
     * @param punts son els punts que s'han aconseguit en la partida.
     * @param filepath es la ruta a l'arxiu de moviments de la partida.
     * @param pic_expectadors es el pic màxim d'expectadors que ha tingut la partida.
     */
    public void createMatch(int id_jugador, int punts, String filepath, int pic_expectadors) {
        conn.insertQuery("INSERT INTO Partida ("+PARTIDA_COLUMN_NAMES+") VALUES ("+id_jugador+","+punts+",'"+filepath+"', CURDATE(),"+pic_expectadors+");");
    }

    /**
     * Actualitza les dades d'un jugador.
     * @param nickname
     * @param correu
     * @param password
     * @param connected
     * @param num_partides
     * @param punts
     * @param ascii_move_left
     * @param ascii_move_right
     * @param ascii_move_down
     * @param ascii_turn_left
     * @param ascii_turn_right
     * @param ascii_pause
     */
    public void updatePlayer( String nickname, String correu, String password, boolean connected, int num_partides, int punts, char ascii_move_left, char ascii_move_right, char ascii_move_down, char ascii_turn_left, char ascii_turn_right, char ascii_pause) {
        conn.updateQuery("UPDATE Jugador set correu = '"+correu+"', contrasenya = '"+password+"', estat_connectat = "+connected+", ultim_login = CURDATE(), num_partides ="+num_partides+", punts = "+punts+", ascii_move_left = '" +ascii_move_left+"', ascii_move_right ='" +ascii_move_right+"', ascii_move_down ='"+ ascii_move_down+"', ascii_turn_left ='"+ ascii_turn_left+"', ascii_turn_right ='"+ ascii_turn_right+"', ascii_pause ='" +ascii_pause+"' where nickname = " + nickname+ ";");
    }

    /**
     *
     * @param nickname
     * @param pass
     * @return
     * @throws Exception
     */
    public String findPlayer(String nickname, String pass) throws Exception {
        //System.out.println("SELECT * FROM jugador WHERE user = \'" + nickname + "\' AND contrasenya = \'" + pass + "\';");
        ArrayList<ArrayList<String>> result = query("SELECT nickname FROM Jugador WHERE nickname = '" + nickname + "' AND contrasenya = '" + pass + "';");
        return (result.get(0).get(0));
    }

    public void deletePlayerByName(String username){
        conn.deleteQuery("DELETE FROM Jugador WHERE nickname = '" + username + "';");
    }

    /**
     * Mètode que es crida per agafar les dades dels usuaris registrats a la vista del servidor.
     * @return array d'Strings de dues dimensions, cada fila és un usuari diferent
     */
    public String[][] getRegisteredPlayers(){

        try {
            ArrayList<ArrayList<String>> result = query("SELECT estat_connectat, nickname, data_registre, ultim_login, num_partides, punts FROM jugador;");

            if (result.size() > 0) {
                String[][] registeredPlayers = new String[result.size()][result.get(0).size()];

                for (int i = 0; i < result.size(); i++) {
                    ArrayList<String> row = result.get(i);

                    // Perform equivalent `toArray` operation
                    String[] copy = new String[row.size()];
                    for (int j = 0; j < row.size(); j++) {
                        // Manually loop and set individually
                        copy[j] = row.get(j);
                    }

                    registeredPlayers[i] = copy;
                }

                return registeredPlayers;
            }

        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Error al buscar els jugadors registrats");
        }
        return null;
    }

    /**
     * Fa una query amb l'email i retorna el nom d'usuari
     * @param email String amb l'email
     * @return String amb el nom d'usuari
     */
    public String getUsernameByEmail(String email){
        ArrayList<ArrayList<String>> result = null;
        try {
            result = query("SELECT nickname FROM Jugador WHERE correu = '" + email + "';");
        } catch (Exception e){
            e.printStackTrace();
        }
        if (result.size() == 0){
            return "";
        } else {
            return result.get(0).get(0);
        }
    }

    /**
     * Fa una query Update a la base de dades per actualitzar l'estat del jugador de connectat a desconnectat
     *  i al revés
     * @param connected boolean per indicar connectat o desconnectat
     * @param username String amb el nom de l'usuari que volem actualitzar
     */
    public void changePlayerState(boolean connected, String username){
        ArrayList<ArrayList<String>> result = null;

        try {
            result = query("SELECT nickname FROM Jugador WHERE nickname = '" + username + "';");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (connected){

            System.out.println("Usuari " + username + " s'ha connectat");
            conn.updateQuery("UPDATE Jugador SET estat_connectat = '1', ultim_login = NOW() WHERE nickname = '" + result.get(0).get(0) + "';");
        } else {
            conn.updateQuery("UPDATE Jugador SET estat_connectat = '0' WHERE nickname = '" + result.get(0).get(0) + "';");
        }
    }

    /**
     * Posa un client a desconnectat
     * @param nickname String amb el nom d'usuari
     */
    public void disconnectPlayer(String nickname){
        conn.updateQuery("UPDATE Jugador SET estat_connectat = '0' WHERE nickname = '" + nickname + "';");
    }

    /**
     * Desconnecta tots els usuaris, va bé per quan es tanca el client
     */
    public void disconnectAllPlayers(){
        conn.updateQuery("UPDATE Jugador SET estat_connectat = '0';");
    }

    /**
     * Comprova que existeixi un usuari a la base de dades i retorna si existeix o no en una string
     * @param username String amb el nom d'usuari
     * @return String amb informació sobre si existeix o no
     */
    public String checkExistentUser(String username){
        try {
            ArrayList<ArrayList<String>> result = query("SELECT estat_connectat, nickname, data_registre, ultim_login, num_partides, punts FROM jugador WHERE nickname = '" + username + "';");
            if (result.size() > 0){
                return "Nom d'usuari en ús";
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Canvia la configuració de les tecles d'un usuari a la base de dades
     * @param newKeys ArrayList<String> tecles a registrar
     * @param username nom d'usuari a qui canviar la configuració de les tecles
     */
    public void changeAssignedKeys(ArrayList<String> newKeys, String username) {
        try {
            ArrayList<ArrayList<String>> result = query("SELECT nickname FROM Jugador WHERE nickname = '" + username + "';");
            conn.updateQuery("UPDATE Jugador SET ascii_move_left ='" + newKeys.get(0) + "', ascii_move_right = '" + newKeys.get(1) + "', ascii_move_down = '" + newKeys.get(2) + "', ascii_turn_left ='" + newKeys.get(3) + "', ascii_turn_right = '" + newKeys.get(4) + "', ascii_pause = '" + newKeys.get(5) + "' WHERE nickname = '" + result.get(0).get(0) + "';");
        } catch (Exception e) {
            System.out.println("La query no ha retornat resultats");
        }
    }

    /**
     * Retorna el màxim de punts que ha fet tothom i el seu nom
     * @return StatisticSet amb les dades
     */
    public StatisticSet getTopPoints() {
        StatisticSet set = new StatisticSet();
        try {
            ArrayList<ArrayList<String>> queryResult = query("select nickname_jugador, punts from Partida WHERE data_inici >= curdate() - INTERVAL dayofweek(curdate())+7 DAY AND data_inici <= curdate() ORDER BY punts DESC LIMIT 10;");
            ArrayList<String> names = new ArrayList<String>();
            ArrayList<Double> points = new ArrayList<>();
            for (int i = 0; i < queryResult.size(); i++) {
                points.add((double)Integer.parseInt(queryResult.get(i).get(1)));
                names.add(queryResult.get(i).get(0));
            }
            set.setValues(names, points);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }

    /**
     * Retorna el màxim d'espectadors d'una partida i el nom del seu jugador
     * @return StatisticSet amb les dades
     */
    public StatisticSet getTopViewers() {
        StatisticSet set = new StatisticSet();
        try {
            ArrayList<ArrayList<String>> queryResult = query("select nickname_jugador, pic_maxim_espectadors from Partida WHERE data_inici >= curdate() - INTERVAL dayofweek(curdate())+7 DAY AND data_inici <= curdate() ORDER BY pic_maxim_espectadors DESC LIMIT 10;");

            ArrayList<String> names = new ArrayList<String>();
            ArrayList<Double> viewers = new ArrayList<>();
            for (int i = 0; i < queryResult.size(); i++) {
                viewers.add((double)Integer.parseInt(queryResult.get(i).get(1)));
                names.add(queryResult.get(i).get(0));
            }
            set.setValues(names, viewers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }

    /**
     * Guarda una partida nova cada cop que s'acaba
     * @param user String amb el nom d'usuari
     * @param score Puntuació feta a la partida
     * @param maxSpectators Màxim d'espectadors simultànis
     * @return Retorna l'id de la partida si s'ha fet bé o -1 si falla.
     */
    public int saveGame(String user, int score, int maxSpectators) {
        conn.insertQuery("INSERT INTO Partida (id_partida, nickname_jugador, punts, data_inici, pic_maxim_espectadors) VALUES (null, '" + user + "'," + score +", NOW(), "+ maxSpectators +");");

        try {
            return Integer.parseInt(query("SELECT COUNT(id_partida) FROM Partida;").get(0).get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }
}