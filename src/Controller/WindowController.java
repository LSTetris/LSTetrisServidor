package Controller;

import Database.DatabaseManager;
import Model.MatchFileManager;
import Model.StatisticSet;
import Model.User;
import Model.UserManager;
import Network.DedicatedServer;
import Network.NetworkManager;
import View.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * La classe WindowController permet gestionar la comunicació entre les vistes y el model del servidor LSTetris.
 * @author C1
 */
public class WindowController implements ActionListener {

    private MainView mainView;
    private AdmUsrView admUsrView;
    private GraphView graphView;
    private MatchFileManagerView matchFileManagerView;
    private MatchFileManager matchFileManager;

    private NetworkManager networkManager;
    private DatabaseManager databaseManager;

    /**
     * Crea un nou WindowController a partir de tots els objectes que calen pel seu correcte funcionament.
     * @param networkManager es el gestor de xarxa del servidor.
     * @param databaseManager es el gestor de base de dades del servidor.
     * @param mainView es la vista principal del servidor.
     * @param admUsrView es la vista d'administració d'usuaris.
     * @param graphView es la vista d'estadistiques de les partides.
     */
    public WindowController(NetworkManager networkManager, DatabaseManager databaseManager, MainView mainView, AdmUsrView admUsrView, GraphView graphView, MatchFileManager matchFileManager, MatchFileManagerView matchFileManagerView){
        this.networkManager = networkManager;
        this.databaseManager = databaseManager;
        this.mainView = mainView;
        this.admUsrView = admUsrView;
        this.graphView = graphView;
        this.matchFileManagerView = matchFileManagerView;
        this.matchFileManager = matchFileManager;

        //Carregar la info dels usuaris a la vista
        if (databaseManager.getRegisteredPlayers() != null){
            admUsrView.loadUserData(databaseManager.getRegisteredPlayers());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case MainView.ACTCMD_BTN_ADM:
                admUsrView.setVisible(true);
                //networkManager.broadcastAll("hola");
                //Update info from model
                break;
            case MainView.ACTCMD_BTN_STAT:
                StatisticSet topViewers = databaseManager.getTopViewers();
                StatisticSet topPoints = databaseManager.getTopPoints();

                graphView.setVisible(true);

                graphView.update(topPoints, topViewers);
                graphView.repaint();
                graphView.revalidate();
                //Update info from model
                break;
            case MainView.ACTCMD_BTN_FLS:
                matchFileManagerView.setVisible(true);
                break;
            case AdmUsrView.ACTCMD_DEL_USER:
                //Obtenim la fila eleccionada i la borrem de la base de dades amb el nom que té l'user d'aquella fila
                //Es té en compte que si no hi ha cap fila seleccionada el mètode del JTable ens retorna un -1
                if (admUsrView.getSelectedRow() != -1){
                    databaseManager.deletePlayerByName(admUsrView.getRowName(admUsrView.getSelectedRow()));
                    //La borrem també de la vista
                    admUsrView.deleteRowById(admUsrView.getSelectedRow());
                }
                break;

            case MatchFileManagerView.ACTCMD_BTN_LD:

                //Apretat el botó per carregar les partides d'un usuari, carreguem els fitxers amb el fileManager
                matchFileManager.loadUserMatches(matchFileManagerView.getSelectedElement());
                //Actualitzem la vista
                matchFileManagerView.loadUserMatches(matchFileManager.getFilenames(), matchFileManagerView.getSelectedElement());
                break;
            case MatchFileManagerView.ACTCMD_BTN_BCK:

                //Apretat el botó per anar enrere des de la vista d'un usuari, tornem a carregar els directoris d'usuaris
                matchFileManager.loadUsers();
                //Actualitzem la vista
                matchFileManagerView.loadUsers(matchFileManager.getFilenames());
                break;
            case MatchFileManagerView.ACTCMD_BTN_DEL:
                //Apretat el botó d'eliminar una partida, provem d'eliminar
                if (matchFileManagerView.getSelectedElement() != null) {
                    try {
                        matchFileManager.deleteFile(matchFileManagerView.getSelectedElement());
                        //Actualitzem la vista
                        matchFileManagerView.loadUserMatches(matchFileManager.getFilenames(), matchFileManager.getActiveUserDirectory());
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(matchFileManagerView, "Problema al borrar el fitxer, comprova si el fitxer existeix", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    //Si no ha seleccionat cap element li mostrem un missatge informatiu
                    JOptionPane.showMessageDialog(matchFileManagerView, "Selecciona un fitxer primer", "Error", JOptionPane.WARNING_MESSAGE);
                }

                break;
            case MatchFileManagerView.ACTCMD_BTN_REF:
                //Si ens apreten el botó de refrescar primer mirem quin és el directori on estem (si estem en el d'un jugador o al d'usuaris)
                //El mètode de carregar les partides d'un usuari i el de carregar els usuaris és diferent perquè la vista ha de fer coses diferents depenent el cas
                if(matchFileManager.getActiveUserDirectory().equals("")){
                    matchFileManager.loadUsers();
                    matchFileManagerView.loadUsers(matchFileManager.getFilenames());
                } else {
                    //
                    matchFileManager.loadUserMatches(matchFileManager.getActiveUserDirectory());
                    matchFileManagerView.loadUserMatches(matchFileManager.getFilenames(), matchFileManager.getActiveUserDirectory());
                }
                break;
        }
    }

    public void updateRegister(String order){
        mainView.updateRegister(order);
    }

    /**
     * Mètode que registra un nou usuari a la base de dades
     * @param user Nova petició d'usuari
     * @param ds Paràmetre que ens indica de quin client ve la petició
     */
    public void registerNewUser(User user, DedicatedServer ds) {

        //Tornar a fer les mateixes validacions que al client
        String errors = UserManager.validateRegisterParameters(user);
        errors += databaseManager.checkExistentUser(user.getNickname());

        if(!errors.isEmpty()){
            //Enviar els errors al client
            ds.sendObject(new String[]{"Register", "Error"}, errors);
        } else {
            //Registrar a la base de dades i enviar el missatge al client conforme és satisfactori
            databaseManager.createUser(user.getNickname(), user.getEmail(), user.getPassword());
            ds.sendObject(new String[]{"Register", "Successful"}, user.getNickname());
            admUsrView.clearTable();
            admUsrView.loadUserData(databaseManager.getRegisteredPlayers());
            new File("matches" + File.separator + user.getNickname()).mkdirs();
        }
    }

    /**
     * Mètode que es crida per donar permís a un usuari per iniciar sessió
     * @param data Dades de inici de sessió
     * @param ds Paràmetre que ens indica de quin client ve la petició
     */
    public void loginUser(String[] data, DedicatedServer ds) {
        try {
            String result = databaseManager.findPlayer(data[0], data[1]);
            if (result != null){
                //Enviem el missatge al client que l'inici és satisfactori
                ds.sendObject(new String[]{"Login", "Successful"}, result);
                databaseManager.changePlayerState(true, data[0]);
                admUsrView.clearTable();
                admUsrView.loadUserData(databaseManager.getRegisteredPlayers());

            } else {
                ds.sendObject(new String[]{"Login", "Error"}, "Revisa les dades");
            }
        } catch (Exception e) {
            ds.sendObject(new String[]{"Login", "Error"}, "Error inesperat");
        }
    }


    /**
     * Mètode que actualitza l'estat de connexió de l'usuari i actualitza la taula de la vista
     * @param id
     */
    public void disconnectUser(String id){
        databaseManager.disconnectPlayer(id);
        admUsrView.clearTable();
        admUsrView.loadUserData(databaseManager.getRegisteredPlayers());
    }

    public ArrayList<String> getFileNames(String s) {
        return matchFileManager.returnUserMatches(s);
    }
}
