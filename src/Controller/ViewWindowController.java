package Controller;

import Database.DatabaseManager;
import Network.NetworkManager;
import View.MainView;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Permet gestionar la comunicació entre els esdeveniments de la finestra i el model.
 * @author C1
 */
public class ViewWindowController implements WindowListener {
    private NetworkManager networkManager;
    private DatabaseManager databaseManager;

    /**
     * Crea un nou controlador de finestra a partir de un NetworkManager i un DatabaseManager.
     * @param server es el servidor que s'utilitzarà per crear el controlador.
     * @param db es el servidor que s'utilitzarà per crear el controlador.
     */
    public ViewWindowController(NetworkManager server, DatabaseManager db){
        this.networkManager = server;
        this.databaseManager = db;
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Gestiona l'esdeveniment de tancament de la finestra.
     * @param e es l'esdeveniment de tancament de la finestra
     */
    @Override
    public void windowClosing(WindowEvent e) {
        if( e.getWindow() instanceof MainView){
            System.out.println("Closing server, telling connected clients");
            networkManager.stopServerConnection();
            databaseManager.disconnectAllPlayers();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
